module.exports = {
   web: {
      address: 'https://www.tunecakes.com',
   },
   api: {
      host: '0.0.0.0'
   },
   trust_proxy: true,
   facebook: {
      clientID: '263421307395016',
      clientSecret: '19e470f7bc47f615e3047eb5ffa05026'
   },
   getstream: {
      "clientKey": "y3jy3wspzwnd",
      "clientSecret": "", // in heroku ENV
      "clientID": "15594"
   },
   twitter: {
      callback: 'https://www.tunecakes.com'
   },
   elastic: {
      index: 'heroku-models',
   },
   cors: {
      origin: 'https://www.tunecakes.com',
      credentials: false
   }
};
