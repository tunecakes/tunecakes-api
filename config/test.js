var path = require('path');
var rootPath = path.normalize(__dirname + '/../..');
var dataDir = rootPath;
var distDir = dataDir + '/dist';

module.exports = {
   // allowed_emails will be defined dynamically in tests
   allowed_emails: [],
   session_key: 'whatchamacallit',
   jwt_key: 'thisisareallylongkeydoe',

   api: {
      host: 'localhost',
      port: 3002,
      models: rootPath + '/models'
   },

   /*
    * Database and datastore connections
    */
   db: 'mongodb://mongo/tunecakes_test',
   elastic: {
     index: 'models',
     connection: {
        host: 'https://site:0299997236a59e67e2c3f611c0662167@bofur-us-east-1.searchly.com'
     }
   },
   /*
    * External Resources
    */
   getstream: {
      clientID: "6349",
      clientKey: "dkrzh7tdcvwm",
      clientSecret: "tf6s7jhzkedecs5vmy6k88p6pfydu2282pyhe72bn2pf9ueqs56vgqacpauyjvew"
   },
};
