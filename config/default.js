var path = require('path');
var rootPath = path.normalize(__dirname + '/..');
var dataDir = rootPath;
var distDir = dataDir + '/dist';

module.exports = {
   app: {
      name: 'TuneCakes'
   },
   allowed_emails: [
      'brian@tunecakes.com',
      'aiguo.fernandez@gmail.com',
      'schultz100190@gmail.com',
      'sebastian.fg@tunecakes.com'
   ],
   default_user: {
      email: 'dork@tunecakes.com',
      password: 'asdfasdf'
   },
   trust_proxy: false,
   session_key: 'whatchamacallit',
   jwt_key: 'thisisareallylongkeydoe',
   api: {
      host: 'localhost',
      port: 3001,
      models: rootPath + '/models'
   },
   web: {
      address: 'http://localhost:3000',
      host: 'localhost',
      port: 3000
   },

   reminder_cron: '00 0,30 * * * *',

   /*
    * Directory and path information
    */
   root: rootPath,
   distribution_directory: distDir,
   log_directory: rootPath + '/log',

   /*
    * Log config
    */
   log_level: 'info',

   /*
    * Database and datastore connections
    */
   db: 'mongodb://localhost/tunecakes',
   elastic: {
      index: 'models',
      connection: {
         host: 'http://localhost:9200'
      }
   },

   /*
    * External Resources
    */
   getstream: {
      clientID: "6073",
      clientKey: "umtdb69urt5w",
      clientSecret: "bryd8em5zm7p2sc6jqdta6wthzat235wn8jv83se42v799gysc6xnh4uuy6xpj5k"
   },
   facebook: {
      clientID: "608947692502876",
      clientSecret: "dbd885460745fdb1d6fcbb665c28b58c",
      callbackURL: "http://localhost:3000/authz/facebook/callback"
   },
   twitter: {
      clientID: 'pFg6hHE3PxY5yhNRlJ5KAsBzr',
      clientSecret: 'P0c77L6B3T1ELP89KImXNyO9cOkrPxIx5geAbDzzWcmsVjXXkB',
      callback: 'http://localhost:3000'
   },
   github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/auth/github/callback'
   },
   google: {
      clientID: "APP_ID",
      clientSecret: "APP_SECRET",
      callbackURL: "http://localhost:3000/auth/google/callback",
      apiKey: "AIzaSyDVW_zAu8f2-LnOa0D0O1WW1CGl6NFPOlM"
   },
   linkedin: {
      clientID: "CONSUMER_KEY",
      clientSecret: "CONSUMER_SECRET",
      callbackURL: "http://localhost:3000/auth/linkedin/callback"
   },
   spotify: {
      clientID: 'db7d7a5e2e984e6eac40b5b492fb46d9',
      clientSecret: 'f0afdf922a094d08a5b428bec5084cdb',
      callbackURL: 'http://localhost:3000/spotify/callback'
   },
   emailer: {
      invites: {
         host: 'mail.gandi.net',
         port: 587,
         email: 'invites@tunecakes.com',
         password: 'thisisbandfan42'
      }
   },
   cors: {
      origin: true,
      credentials: true
   }
};
