var path = require('path');
var rootPath = path.normalize(__dirname + '/../..');
var dataDir = process.env.OPENSHIFT_DATA_DIR;
var distDir = dataDir + '/dist';

module.exports = {
   allowed_emails: ['schultz100190@gmail.com', 'aiguo.fernandez@gmail.com', 'sebastian.fg@tunecakes.com', 'test_user_1@test.com', 'test_user_2@test.com', 'test_user_3@test.com', 'test_user_4@test.com'],
   session_key: 'r6Su0xOVMC1kZzxKD236dM9BWa5JQQUd',
   jwt_key: '0ugHX12bhx6cp8S42vOtY2yciy4z84N2',
   api: {
      host: process.env.OPENSHIFT_NODEJS_IP,
      port: process.env.OPENSHIFT_NODEJS_PORT,
      models: rootPath + '/models'
   },
   web: {
      host: process.env.TUNECAKES_WEB_HOST || 'localhost',
      port: process.env.TUNECAKES_WEB_PORT || 3000
   },

   /*
    * Directory and path information
    */
   root: rootPath,
   distribution_directory: distDir,
   log_directory: process.env.OPENSHIFT_LOG_DIR,

   /*
    * Database and datastore connections
    */
   db: 'mongodb://' + process.env.OPENSHIFT_MONGODB_DB_USERNAME + ':' +
      process.env.OPENSHIFT_MONGODB_DB_PASSWORD + '@' +
      process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
      process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
      process.env.OPENSHIFT_APP_NAME,
   elastic: {
      index: 'models',
      connection: {
         host: 'https://site:2b1be15c2047f5ce8eedb378feccc28f@fili-us-east-1.searchly.com'
      }
   },
   
   getstream: {
      clientID: process.env.GET_STREAM_CLIENT_ID,
      clientKey: process.env.GET_STREAM_CLIENT_KEY,
      clientSecret: process.env.GET_STREAM_CLIENT_SECRET
   }
};
