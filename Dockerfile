FROM alpine:3.6

WORKDIR $HOME/src/app
COPY ./ $HOME/src/app

RUN echo "ipv6" >> /etc/modules

RUN apk add --no-cache --update nodejs nodejs-npm git python make g++ gcc \
   && npm install --production --build-from-source=bcrypt

RUN npm run gen_cev

CMD ["npm", "start"]
