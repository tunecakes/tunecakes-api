SHELL := /bin/bash

API_TAG=tunecakes/api:0.0.1
API_NAME=tunecakes-api
API_PORT=3001
API_SOURCE_DIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))src/
TC_NETWORK=tunecakes-net
DOCKER_CMD=docker
ELASTIC_CONFIG = ${API_SOURCE_DIR}elasticsearch.yml
ELASTICSEARCH_HOST ?= http://elastic:9200
ELASTIC_PORT=9200
ELASTIC_NAME=elastic
ELASTIC_CONTAINER=elasticsearch
MONGO_DATA_DIR=${API_SOURCE_DIR}.db
MONGO_NAME=mongo
MONGO_CONTAINER=mongo
HEROKU_TAG=registry.heroku.com/tunecakes-api-staging/web:latest

.PHONY: build check_local_config clean logs follow remove_stopped_elastic remove_stopped_mongo rm run ssh start_elastic start_mongo start_network test

default: build

build:
	${DOCKER_CMD} build -t ${API_TAG} ./

build-no-cache:
	${DOCKER_CMD} build --no-cache -t ${API_TAG} ./

.PHONY: build-heroku
build-heroku:
	sudo rm -rf ./.db
	sudo rm -rf ./node_modules
	sudo rm -rf ./log/*
	${DOCKER_CMD} build -t ${HEROKU_TAG} .

.PHONY: push-heroku
push-heroku:
	${DOCKER_CMD} push ${HEROKU_TAG}

check_local_config:
ifeq ("$(wildcard ${API_SOURCE_DIR}../config/local-development.js)","")
	@echo config/local.js does not exist, starting create script...
	npm install prompt
	node src/scripts/check_local.js
	rm -rf ${API_SOURCE_DIR}node_modules
else
	@echo config/local-development.js exists. Skipping config script...
endif

clean:
ifeq ($(shell ${DOCKER_CMD} images | grep "^<none>" | wc -l),0)
	@echo No untagged docker images to remove...
else
	@echo Cleaning untagged docker images...
	${DOCKER_CMD} rmi $(shell ${DOCKER_CMD} images | grep "^<none>" | awk '{print $$3}')
endif

logs:
	${DOCKER_CMD} logs -f ${API_NAME}

.PHONY: init_elastic
init_elastic:
	${DOCKER_CMD} run \
		--network ${TC_NETWORK} \
		-e TUNECAKES_ELASTIC_CONNECTION_HOST=${ELASTICSEARCH_HOST} \
		${API_TAG} node src/scripts/init_elastic.js

.PHONY: reindex_elastic
reindex_elastic:
	${DOCKER_CMD} run -it \
		--network ${TC_NETWORK} \
		-e TUNECAKES_ELASTIC_CONNECTION_HOST=${ELASTICSEARCH_HOST} \
		${API_TAG} /bin/sh -c "npm install prompt; node src/scripts/reindex_elastic.js;"

remove_stopped_elastic:
ifeq ($(shell ${DOCKER_CMD} ps -f="status=exited" -f="name=${ELASTIC_NAME}"| wc -l),2)
	@echo Removing exited Elasticsearch container
	${DOCKER_CMD} rm -f ${ELASTIC_NAME} || true
else
	@echo No exited Elasticsearch containers to remove
endif

remove_stopped_mongo:
ifeq ($(shell ${DOCKER_CMD} ps -f="status=exited" -f="name=${MONGO_NAME}"| wc -l),2)
	@echo Removing exited MongoDB container
	${DOCKER_CMD} rm -f ${MONGO_NAME} || true
else
	@echo No exited MongoDB containers to remove
endif

rm:
ifeq ($(shell ${DOCKER_CMD} ps --filter="name=${API_NAME}" -a | wc -l),2)
	${DOCKER_CMD} rm -f ${API_NAME}
else
	@echo "tunecakes-api doesn't exist. Skipping rm..."
endif

run: start_network rm check_local_config start_mongo start_elastic init_elastic
	${DOCKER_CMD} run --name ${API_NAME} -d \
		--network ${TC_NETWORK} \
		-e TUNECAKES_API_HOST=0.0.0.0 \
		-e TUNECAKES_DB='mongo/tunecakes' \
		-e TUNECAKES_ELASTIC_CONNECTION_HOST='http://elastic:9200' \
		-e NODE_ENV='local' \
		-p ${API_PORT}:${API_PORT} \
		-v ${API_SOURCE_DIR}../config:/src/app/config \
		-v ${API_SOURCE_DIR}:/src/app/src \
		${API_TAG} npm run debug

debug: rm check_local_config start_mongo start_elastic
	${DOCKER_CMD} run --name ${API_NAME} -it \
		--network ${TC_NETWORK} \
		-e TUNECAKES_API_HOST=0.0.0.0 \
		-e TUNECAKES_DB='mongo/tunecakes' \
		-e TUNECAKES_ELASTIC_CONNECTION_HOST='http://elastic:9200' \
		-e NODE_ENV='local' \
		-p ${API_PORT}:${API_PORT} \
		-v ${API_SOURCE_DIR}../config:/src/app/config \
		-v ${API_SOURCE_DIR}:/src/app/src \
		${API_TAG} \
		node debug ./src/app.js # This needs to be last

ssh:
	${DOCKER_CMD} exec -it ${API_NAME} /bin/sh

test: start_mongo
	${DOCKER_CMD} run -it \
		--network ${TC_NETWORK} \
		-e TUNECAKES_ELASTIC_CONNECTION_HOST='http://elastic:9200' \
		-e NODE_ENV='test' \
		${API_TAG} npm test

.PHONY: test_local
test_local: start_mongo
	${DOCKER_CMD} run -it \
		--network ${TC_NETWORK} \
		-e TUNECAKES_ELASTIC_CONNECTION_HOST='http://elastic:9200' \
		-e NODE_ENV='test' \
		-v ${API_SOURCE_DIR}../config:/src/app/config \
		-v ${API_SOURCE_DIR}:/src/app/src \
		${API_TAG} npm test

start_elastic: remove_stopped_elastic start_network
ifeq ($(shell ${DOCKER_CMD} ps --filter="name=${ELASTIC_NAME}"| wc -l),2)
	@echo Elasticsearch container is running
else
	@echo Starting Elasticsearch container
	${DOCKER_CMD} run --name ${ELASTIC_NAME} \
		--network ${TC_NETWORK} \
		-p ${ELASTIC_PORT}:${ELASTIC_PORT} -d \
		${ELASTIC_CONTAINER}
endif
	@echo Waiting for Elasticsearch to start
	@until curl --output /dev/null --silent --head --fail http://localhost:${ELASTIC_PORT}; \
	do \
		printf '.'; \
		sleep 2; \
	done; \
	printf "\n"
	@echo Elasticsearch is running!


start_mongo: start_network remove_stopped_mongo
	mkdir -p ${MONGO_DATA_DIR}
ifeq ($(shell ${DOCKER_CMD} ps --filter="name=${MONGO_NAME}"| wc -l),2)
	@echo MongoDB container is running
else
	@echo Starting MongoDB container
	${DOCKER_CMD} run --name ${MONGO_NAME} \
		-p 27017:27017 \
		--network ${TC_NETWORK} \
		-v ${MONGO_DATA_DIR}:/data/db \
		-d ${MONGO_CONTAINER}
endif

start_network:
	${DOCKER_CMD} network create ${TC_NETWORK} 2> /dev/null || echo 'Already started tunceakes-net'
