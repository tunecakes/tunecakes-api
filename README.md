

to install deps:
`npm install`

to cleanup deps:
`npm prune`

to startup app:
`npm start`

to run tests:
`npm test`
