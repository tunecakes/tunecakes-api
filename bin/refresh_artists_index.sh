#! /bin/bash

user=${user:-brian@tunecakes.com}
pass=${pass:-asdfasdf}

echo $user
echo $pass

function strip_quotes() {
    printf `sed -e 's/^"//' -e 's/"$//' <<< "$1"`
}

echo "Signing in..."
cookie=`curl 'http://localhost:3001/api/authenticate' -H 'Content-type: application/json' \
        -d "{\"email\": \"$user\", \"password\": \"$pass\"}" | jq '.token'`
cookie=`strip_quotes $cookie`

echo "Successfully signed in."

echo "Grabbing artists"
artists=`curl localhost:3001/api/artist -H "Authorization: Bearer $cookie" | jq '.[].alias'`

for artist in $artists; do
    artist=`strip_quotes $artist`
    echo "Touching artist $artist"
    curl -XPUT localhost:3001/api/artist/$artist -H "Authorization: Bearer $cookie" \
         -H "Content-type: application/json" -d '{}'
done
