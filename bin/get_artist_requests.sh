#! /bin/bash

API_HOST=${API_HOST:-https://api.tunecakes.com}
PAGE=${PAGE:-0}

# EXAMPLE DATA
#  {
#    "created": "2017-11-27T01:19:33.623Z",
#    "user_origin": "5a1b654355e328002058ad55",
#    "artist_name": "Yoshida Brothers",
#    "spotify_id": "1C08PKH51P2fcJMrPOFRwI",
#    "href": "/api/request/5a1b682555e328002058ad6e",
#    "_type": "request",
#    "id": "5a1b682555e328002058ad6e"
#  }

curl $API_HOST/api/request?page=$PAGE | jq -r '.[]|"Artist - \(.artist_name)\nSpotify ID - \(.spotify_id)\n"'
