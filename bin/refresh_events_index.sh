#! /bin/bash

user=${user:-brian@tunecakes.com}
pass=${pass:-asdfasdf}

echo $user
echo $pass

function strip_quotes() {
    printf `sed -e 's/^"//' -e 's/"$//' <<< "$1"`
}

cookie=`curl 'http://localhost:3001/api/authenticate' -H 'Content-type: application/json' \
        -d "{\"email\": \"$user\", \"password\": \"$pass\"}" | jq '.token'`
cookie=`strip_quotes $cookie`

events=`curl localhost:3001/api/event -H "Authorization: Bearer $cookie" | jq '.[].id'`

for event in $events; do
    event=`strip_quotes $event`
    curl -XPUT localhost:3001/api/event/$event -H "Authorization: Bearer $cookie" \
         -H "Content-type: application/json" -d '{}'
done
