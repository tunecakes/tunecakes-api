var ec = require('../controllers/event.js'),
   eventFeeds = require('../constants').feeds.event,
   FollowMap = require('../mongo/follow_map.js'),
   getstream = require('../util/getstream.js'),
   uc = require('../controllers/user.js');

function addFollowFlags(event, userFeed, flags) {
   flags.forEach(function(flag) {
      var feed = getstream.getFeed(eventFeeds[flag], event.id);
      getstream.follow(userFeed, feed);
   });
}

function removeFollowFlags(event, userFeed, flags) {
   flags.forEach(function(flag) {
      var feed = getstream.getFeed(eventFeeds[flag], event.id);
      getstream.unfollow(userFeed, feed);
   });
}

function addFollower(event, user, flags) {
   var eventFeeds = ec.getFlatFeeds(event);
   var userAggFeed = uc.getAggregateFeed(user);
   var userNotiFeed = uc.getNotificationFeed(user);

   getstream.followMany(userAggFeed, eventFeeds);
   addFollowFlags(event, userNotiFeed, flags);

   var followMap = new FollowMap({
      publisher_type: event._type,
      publisher: event.id,
      follower: user.id,
      flags: flags
   });

   followMap.save();
   return followMap;
}

module.exports = {
   addFollower: addFollower
};
