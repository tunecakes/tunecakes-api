var ac = require('../controllers/artist.js'),
   artistFeeds = require('../constants').feeds.artist,
   FollowMap = require('../mongo/follow_map.js'),
   getstream = require('../util/getstream.js'),
   uc = require('../controllers/user.js');

function addFollowFlags(artist, userFeed, flags) {
   flags.forEach(function(flag) {
      var feed = getstream.getFeed(artistFeeds[flag], artist.id);
      getstream.follow(userFeed, feed);
   });
}

function removeFollowFlags(artist, userFeed, flags) {
   flags.forEach(function(flag) {
      var feed = getstream.getFeed(artistFeeds[flag], artist.id);
      getstream.unfollow(userFeed, feed);
   });
}

function addFollower(artist, user, flags) {
   var artistFeeds = ac.getFlatFeeds(artist);
   var userAggFeed = uc.getAggregateFeed(user);
   var userNotiFeed = uc.getNotificationFeed(user);

   getstream.followMany(userAggFeed, artistFeeds);
   addFollowFlags(artist, userNotiFeed, flags);

   var futureEvents = false;
   var now = new Date();

   for(var i = 0; i < artist.events.length; i++) {
      if(now < artist.events[i].start_date) {
         futureEvents = true;
         break;
      }
   }

   if(futureEvents) {
      var activity = getstream.createActivity('alert', artist, {}, 'future_events');
      getstream.addActivity(activity, userNotiFeed);
   }

   var followMap = new FollowMap({
      publisher_type: artist._type,
      publisher: artist.id,
      follower: user.id,
      flags: flags
   });

   var query = {
      follower: user.id,
      publisher: artist.id
   };

   var data = {
      publisher_type: artist._type,
      publisher: artist.id,
      follower: user.id,
      flags: flags
   };

   return FollowMap.findOneAndUpdate(query, data, {upsert: true});
}

module.exports = {
   addFollower: addFollower
};
