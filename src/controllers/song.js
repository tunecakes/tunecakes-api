var Controller = require('./_base'),
   SongModel = require('../models/song.js'),
   SongMongo = require('../mongo/song.js'),
   log = require('../logging.js').logger,
   async = require('async');

var SC = function () {
   Controller.call(this, SongMongo, SongModel);
};

SC.prototype = Object.create(Controller.prototype);

SC.prototype.parseSongs = function parseSongs(songs, callback) {
   var _this = this;
   var songids = [];
   async.eachSeries(songs, function (song, cb) {
      if (!song.id) {
         _this.postNew(song, function (err, m_song) {
            if (err) return cb(err);
            songids.push(m_song.id);
            cb();
         });
      } else {
         _this.getById(song.id, function (err, popSong) {
            if (err) return cb(err);
            _this.updateDocument(song, popSong, function (err, updatedSong) {
               if (err) return cb(err);
               songids.push(song.id);
               cb();
            });
         });
      }
   }, function (err) {
      if (err) log.error(err);
      callback(songids);
   });
};

module.exports = new SC();
