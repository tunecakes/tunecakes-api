var async = require('async'),
   Controller = require('./_base.js'),
   ArtistController = require('./artist'),
   UserController = require('./user'),
   SongController = require('./song.js'),
   ReleaseModel = require('../models/release.js'),
   ReleaseMongo = require('../mongo/release.js'),
   errors = require('../error'),
   ControllerError = errors.ControllerError;

var RC = function () {
   Controller.call(this, ReleaseMongo, ReleaseModel);
};

RC.prototype = Object.create(Controller.prototype);

RC.prototype.validate = function validate(doc, model, user, callback) {
   // Trying to validate before parsing the songs is a nightmare.
   // We run into array cast error before we even get to the actual
   // validation. So, let's just re-add songs after for now. Once
   // we expand the songs endpoints, then we can do proper song
   // validation
   
   var tmpSongs = doc.songs;
   delete doc.songs;

   Controller.prototype.validate.call(this, doc, model, user, function(err) {
      if(err) return callback(err);
      
      doc.songs = tmpSongs;
      callback();
   });
};

RC.prototype.updateDocument = function updateDocument(doc, model, callback) {
   delete doc.songs;
   delete model.songs;

   this.saveDocument(doc, model, callback);
};

RC.prototype.postNew = function postNew(doc, callback) {
   var owner = doc.owner;
   var _this = this;

   ArtistController.parseDocument(doc.artist, function (err, artist) {
      if (err) return callback(err);
      if (!artist) return callback(new ControllerError('Shit fucked up'));

      doc.artist = artist;

      SongController.parseSongs(doc.songs, function (songids) {
         doc.songs = songids;

         Controller.prototype.postNew.call(_this, doc, function(err, release) {
             if(err) return callback(err);
            
             ArtistController.releaseCreatedActivity(artist, release);

             owner.owned_releases.addToSet(release);
             owner.save();

             callback(null, release);
         });
      });
   });
};

module.exports = new RC();
