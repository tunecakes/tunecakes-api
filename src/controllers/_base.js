/*global  */
var async = require('async'),
   constants = require('../constants'),
   perm = constants.permissions,
   MongoError = require('../error').MongoError,
   ControllerError = require('../error').ControllerError,
   FollowMapMongo = require('../mongo/follow_map.js'),
   DeleteMongo = require('../mongo/delete.js'),
   log = require('../logging.js').logger,
   getstream = require('../util/getstream.js'),
   _ = require('underscore'),
   feed_constants = constants.feeds,
   util = require('../util/general.js');

function Controller(mongo, model) {
   this.Mongo = mongo;
   this.model = model;
}

Controller.prototype = {
   /*
    * Returns the active user's role based on the given Document and User
    */
   getUserRole: function getUserRole(model, user) {
      var userRole = perm.roles.USER;

      if (!user) return userRole;

      if (!model) return perm.NONE;

      if(user.email === "dork@tunecakes.com") return perm.roles.OWNER;

      _.forEach(model.admins, function (admin) {
         var adminId = admin._id || admin;
         if (adminId.equals(user.id)) userRole = perm.roles.ADMIN;
      });

      if (model.owner) {
         var ownerId = model.owner._id || model.owner;
         if (ownerId == user.id) userRole = perm.roles.OWNER;
      }

      if (this.model.name === 'user')
         if (model._id && model._id == user.id) userRole = perm.roles.OWNER;

      log.debug('User ' + user.id + ' role is ' + userRole);
      return userRole;
   },

   filterDocument: function filterDocument(doc, user) {
      var _this = this;
      var role = _this.getUserRole(doc, user);

      _.forEach(doc, function (value, key) {
         var perms = _this.model.permissions;
         if (perms[key] && perms[key] === perm.NONE) {
            delete(doc[key]);
         }
      });

      return doc;
   },

   parseDocument: function parseDocument(doc, callback) {
      log.error(this.model.name + '\'s controller does not have parseDocument defined');
      callback(new ControllerError('There was an error'));
   },

   parseDocumentArray: function parseDocumentArray(docArray, successCallback, failedCallback, finishedCallback) {
      var _this = this;
      var parsedDocs = [];
      var failedDocs = [];

      if (!docArray || docArray.length < 1) return finishedCallback([], []);

      async.forEach(docArray, function (doc, cb) {
         _this.parseDocument(doc, function (err, parsedDoc) {
            if (err || !parsedDoc) {
               failedCallback(err, doc);
               failedDocs.push(doc);
            } else {
               successCallback(parsedDoc);
               parsedDocs.push(parsedDoc);
            }
            cb();
         });
      }, function () {
         if (finishedCallback) finishedCallback(parsedDocs, failedDocs);
      });
   },

   _executeQuery: function _executeQuery(query, callback) {
      var _this = this;
      if (_this.model.populate) query.populate(_this.model.populate);
      query.exec(function (err, doc) {
         if (err) return callback(MongoError.parseError(err));
         if (!doc) return callback(new ControllerError(_this.model.name + ' not found'));

         callback(null, doc);
      });
   },

   /**
    * Retrieves a list of all documents for this model
    *
    */
   getAll: function getAll(query, sort, page, callback) {
      var limit = 25,
         _this = this;
      log.debug('Getting all ' + _this.model.name + ' documents.');


      var q = _this.Mongo
         .find(query)
         .sort(sort)
         .skip(limit * page)
         .limit(limit);
      _this._executeQuery(q, callback);
   },

   /**
    * Find a document by ID. Will send the object as a json response.
    *
    */
   getById: function getById(docId, callback) {
      var _this = this;
      log.debug('Getting ' + _this.model.name + ' by ID.', docId);

      var q = _this.Mongo.findById(docId);
      _this._executeQuery(q, callback);
   },

   /**
    * Find a document by given field.
    *
    */
   getBy: function getBy(searchField, searchValue, callback) {
      var _this = this;
      log.debug('Getting ' + _this.model.name + ' by ' + searchField, searchValue);

      var search = {};
      search[searchField] = searchValue;

      var q = _this.Mongo.findOne(search);
      _this._executeQuery(q, callback);
   },

   /**
    * If the id param is a Mongoose ObjectId, then it will search by id, otherwise
    * it will search for the alias. Will send the object as a json response.
    *
    */
   getByIdOr: function getByIdOr(searchField, searchValue, callback) {
      var _this = this,
         search;

      if (util.isObjectId(searchValue)) {
         search = {
            $or: [{
                  '_id': searchValue
               },
               {
                  [searchField]: searchValue
               }
            ]
         };
      } else {
         search = {
            [searchField]: searchValue
         };
      }

      var q = _this.Mongo.findOne(search);
      _this._executeQuery(q, callback);
   },

   /**
    * Creates a new document for from req.body.
    *
    * The use of the callback here overrides the default express middleware chain.
    * All implementations of the resource object will need to implement their own postNew
    * function and send the regular express callback as callback.next. If it needs its own
    * custom callback, it'll need to be in callback.return.
    *
    */
   postNew: function postNew(doc, callback) {
      log.debug('Posting new ' + this.model.name, doc);

      var model = new this.Mongo();
      this.saveDocument(doc, model, callback);
   },

   findDeletedById: function findDeletedById(docId, callback) {
      DeleteMongo.findById(docId, callback);
   },

   deleteDocument: function deleteDocument(doc, callback) {
      doc.remove();
      callback(null, doc);
   },

   updateDocument: function updateDocument(doc, model, callback) {
      this.saveDocument(doc, model, callback);
   },

   saveDocument: function saveDocument(doc, mongooseObject, callback) {
      mongooseObject = this.verifyFields(doc, mongooseObject);

      // We have alreay run validation
      mongooseObject.save({
         validateBeforeSave: false
      }, function (err) {
         if (err) return callback(MongoError.parseError(err));
         return callback(null, mongooseObject);
      }).catch(function () {});
   },

   verifyFields: function verifyFields(doc, model) {
      // Don't use _.forEach() here because songs have the 'length' property
      var fields = Object.keys(doc);

      fields.forEach(function (key) {
         model[key] = doc[key];
      });

      return model;
   },

   checkAdminPermissions: function checkEditPermissions(doc, model, user) {
      var role = this.getUserRole(model, user);

      if (role !== perm.roles.ADMIN && role !== perm.roles.OWNER) {
         return 'You do not have permission to edit this document';
      }
   },

   checkOwnerPermissions: function checkEditPermissions(doc, model, user) {
      var role = this.getUserRole(model, user);

      if (role !== perm.roles.OWNER) {
         return 'You do not have permission to edit this document';
      }
   },

   filterUpdateDocument: function filterUpdateDocument(doc, model, user) {
      var role = this.getUserRole(model, user);

      function removeKey(key) {
         delete(doc[key]);
      }

      _.each(doc, function (v, k) {
         // We should change how we update upload images and remove this edge case
         if (k === 'encodedImage') return;

         if (!this.model.permissions[role][k]){
            if(role === perm.roles.USER) return removeKey(k);
            
            // If they are an admin or owner, leave it be
            return;
         }
         if (this.model.permissions[role][k] !== 'RW') return removeKey(k);
         if (doc[k] === model[k]) return removeKey(k);
      }, this);
   },

   validate: function validate(doc, model, user, callback) {
      model = this.verifyFields(doc, model);

      model.validate(function (err) {
         if (err) return callback({
            errors: err.errors
         });
         callback();
      });
   },

   /*
    * Follow module
    */
   getFollower: function getFollower(publisher, user, callback) {
      FollowMapMongo.findOne({
         publisher: publisher._id,
         follower: user.id
      }).exec(callback);
   },

   postFollower: function postFollower(flags, publisher, user, callback) {
      var _this = this;
      var followMap = new FollowMapMongo({
         publisher_type: _this.model.name,
         publisher: publisher.id,
         follower: user.id,
         flags: flags
      });

      _.mapObject(feed_constants[_this.model.name], function (feed, flag) {
         //TODO: use getstream batch calls
         if (flag === 'everything') return;
         getstream.followStream(feed_constants.user.aggregated,
            user.id,
            feed,
            publisher.id);
      });

      async.forEach(flags, function (flag) {
         //TODO: use getstream batch calls
         getstream.followStream(feed_constants.user.notifications,
            user.id,
            feed_constants[_this.model.name][flag],
            publisher.id);
      });

      followMap.save();
      callback(null, followMap);
   },

   //   updateFollower: function updateFollower(flags, publisher, user, callback) {
   //      var _this = this,
   //         addedFlags,
   //         removedFlags;
   //
   //      _this.getFollower(publisher, user, function (err, followMap) {
   //         removedFlags = _.difference(followMap.flags, flags),
   //            addedFlags = _.difference(flags, followMap.flags);
   //
   //         async.forEach(removedFlags, function (flag) {
   //            //TODO: use getstream batch calls
   //            getstream.unfollowStream(feed_constants.user.notifications,
   //               user.id,
   //               feed_constants[_this.model.name][flag],
   //               publisher.id);
   //         });
   //
   //         async.forEach(addedFlags, function (flag) {
   //            //TODO: use getstream batch calls
   //            getstream.followStream(feed_constants.user.notifications,
   //               user.id,
   //               feed_constants[_this.model.name][flag],
   //               publisher.id);
   //         });
   //
   //         followMap.flags = flags;
   //         followMap.save();
   //         callback(err, followMap);
   //      });
   //   },

   getFeed: function getFeed(feedType, mongoObject, options, callback) {
      var _this = this;

      var feedName = feed_constants[mongoObject._type][feedType];

      if (!feedName) return callback(new ControllerError('Feed type not recognized'));

      getstream.getFeedActivities(feedName, mongoObject.id, options, function (err, data) {
         log.debug('getstream GET feed response', data);

         if (err) return callback(err);
         _this.populateFeed(data, callback);
      });
   },

   populateFeed: function populateFeed(data, callback) {
      var activities = [];

      if (!data) {
         log.error('There is no data to populate');
         return callback(null, {});
      }

      if (!data.results || !_.isArray(data.results)) {
         log.error('Unable to parse feed data response', data);
         return callback(null, {});
      }

      data.results.forEach(function (result, idx) {
         if (result.activities) {
            // This is an aggregated feed
            if (result.activities.length == 1) {
               var activity = result.activities.pop();
               activity.is_read = result.is_read;
               activity.is_seen = result.is_seen;
               activities.push(activity);
               data.results[idx] = activity;
               delete(result.activities);
            } else {
               activities = _.union(activities, result.activities);
            }
         } else {
            // This is a flat feed
            activities.push(result);
         }
      });

      callback(null, data);
   },

   transformFeedItem: function transformFeedItem(item) {
      throw new Error('transformFeedItem has not been implemented for ' + this.model.name);
   },

   transformAggregatedFeedItem: function transformAggregatedFeedItem(item) {
      throw new Error('transformAggregatedFeedItems has not been implemented for ' + this.model.name);
   }
};

module.exports = Controller;
