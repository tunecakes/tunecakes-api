var async = require('async'),
   mongoose = require('mongoose'),
   Controller = require('./_base'),
   UserController = require('./user'),
   errors = require('../error'),
   geolib = require('geolib'),
   getstream = require('../util/getstream.js'),
   log = require('../logging.js').logger,
   ControllerError = errors.ControllerError,
   ArtistModel = require('../models/artist.js'),
   ArtistMongo = require('../mongo/artist.js'),
   constants = require('../constants.js'),
   feed_constants = constants.feeds;

var AC = function () {
   Controller.call(this, ArtistMongo, ArtistModel);
};

AC.prototype = Object.create(Controller.prototype);

AC.prototype.parseDocument = function parseDocument(jsonArtist, callback) {
   var _this = this;
   // Undefined jsonArtist
   if (!jsonArtist) callback(new ControllerError('There is no artist here'));
   // jsonArtist is a mongoose ObjectId
   else if (jsonArtist instanceof mongoose.Types.ObjectId) _this.getById(jsonArtist.toString(), callback);
   // jsonArtist is a string form of either the alias or ID
   else if (typeof jsonArtist === 'string') _this.getByIdOr('alias', jsonArtist, callback);
   // jsonArtist is an object with ID string
   else if (jsonArtist.id) _this.getById(jsonArtist.id, callback);
   // jsonArtist is an object with alias string
   else if (jsonArtist.alias) _this.getBy('alias', jsonArtist.alias, callback);
   // Well... fuck it
   else callback(new ControllerError('unable to parse artist'));
};

AC.prototype.postNew = function postNew(doc, callback) {
   var _this = this;

   Controller.prototype.postNew.call(_this, doc, function(err, artist) {
      if(err) return callback(err);

      var feeds = _this.getFlatFeeds(artist);
      getstream.followMany(_this.getEverythingFeed(artist), feeds)
      .catch(function(err) {
         log.warn('Failed to init artist ' + artist.id + ' everything feed', err.detail);
      });

      return callback(err, artist);
   });
};

AC.prototype.getFlatFeeds = function getFlatFeeds(artist) {
   return  [
      this.getEventFeed(artist),
      this.getReleaseFeed(artist),
      this.getBlogFeed(artist),
      this.getOtherFeed(artist)
   ];
};

AC.prototype.getAllFeeds = function getFlatFeeds(artist) {
   var feeds = this.getFlatFeeds(artist);
   feeds.push(this.getEverythingFeed(artist));
   return feeds;
};

AC.prototype.updateDocument = function updateDocument(doc, artist, callback) {
   var _this = this;

   Controller.prototype.updateDocument.call(_this, doc, artist, function(err, artist) {
      if(err) return callback(err);

      _this.artistUpdateActivity(doc, artist);
      return callback(err, artist);
   });
};

/**
 * Extend saveDocument to make sure the artist gets added to the user that
 * created it.
 *
 * @param req
 * @param res
 * @param next
 */
AC.prototype.saveDocument = function saveDocument(jsonArtist, mongoArtist, callback) {
   var _this = this;

   async.parallel([function (cb) {
         if (!jsonArtist.owner) return cb();

         UserController.parseDocument(jsonArtist.owner, function (err, owner) {
            // TODO: figure out what to do if there was an error
            _this.setOwner(owner, mongoArtist);
            cb();
         });
      },
      function (cb) {
         if (!jsonArtist.admins) return cb();

         UserController.parseDocumentArray(jsonArtist.admins, function (admin) {
            _this.addAdmin(admin, mongoArtist);
         }, function (err, user) {
            return;
         }, function (admins, failedAdmins) {
            cb();
         });
      }
   ], function () {
      Controller.prototype.saveDocument.call(_this, jsonArtist, mongoArtist, callback);
   });
};

AC.prototype.setOwner = function setOwner(newOwner, artist, callback) {
   log.debug('Setting owner to ' + newOwner + ' for artist ' + artist.name);

   // Remove old owner
   var oldOwner = artist.owner;
   if (oldOwner) {
      if (oldOwner.owned_artists) {
         oldOwner.owned_artists.pull(artist);
         oldOwner.save();
      }
   }

   // Add new owner
   newOwner.owned_artists.addToSet(artist);
   newOwner.save();
   artist.owner = newOwner;

   if (callback) callback();
};

AC.prototype.addAdmin = function addAdmin(newAdmin, artist, callback) {
   newAdmin.admin_artists.addToSet(artist);
   newAdmin.save();
   artist.admins.addToSet(newAdmin);
   if (callback) callback();
};

AC.prototype.removeAdmin = function removeAdmin(oldAdmin, artist, callback) {
   oldAdmin.admin_artists.pull(artist);
   oldAdmin.save();
   artist.admins.pull(oldAdmin);
   if (callback) callback();
};

AC.prototype.addEvent = function addEvent(artist, event) {
   log.info('Adding event (' + event.id + ') to artist ' + artist.name + ' (' + artist.id + ')');

   event.linked_artists.addToSet(artist.id);
   event.save();

   artist.events.addToSet(event.id);
   artist.save();
};

AC.prototype.addRelease = function addRelease(artist, release) {
   log.info('Adding release (' + release.id + ') to artist ' + artist.name + ' (' + artist.id + ')');

   release.artist = artist.id;
   release.linked_artists.addToSet(artist.id);
   release.save();

   artist.releases.addToSet(release.id);
   artist.save();

   this.releaseCreatedActivity(artist, release);
};
/**
 * FEED FUN
 */
AC.prototype.getEverythingFeed = function(artist) {
   return getstream.getFeed(feed_constants.artist.everything, artist.id);
};

AC.prototype.getEventFeed = function(artist) {
   return getstream.getFeed(feed_constants.artist.events, artist.id);
};

AC.prototype.getBlogFeed = function(artist) {
   return getstream.getFeed(feed_constants.artist.blog, artist.id);
};

AC.prototype.getOtherFeed = function(artist) {
   return getstream.getFeed(feed_constants.artist.other, artist.id);
};

AC.prototype.getReleaseFeed = function(artist) {
   return getstream.getFeed(feed_constants.artist.releases, artist.id);
};

AC.prototype.artistUpdateActivity = function(updates, artist) {
   // Delete these. The arrays will always show as updated
   delete(updates.tags);
   delete(updates.members);
   delete(updates.start_date);

   // This object is empty, therefore nothing to report
   if(Object.keys(updates).length < 1) return;

   var activity = getstream.createActivity('updated', artist, updates, 'profile');
   var feed = this.getOtherFeed(artist);
   
   getstream.addActivity(activity, feed);
};

AC.prototype.eventCreatedActivity = function (artist, event) {
   var activity = getstream.createActivity('created', artist, event);
   var feed = this.getEventFeed(artist);

   getstream.addActivity(activity, feed);
};

AC.prototype.blogPostCreatedActivity = function (artist, post) {
   var activity = getstream.createActivity('created', artist, post);
   var feed = this.getBlogFeed(artist);

   getstream.addActivity(activity, feed);
};

AC.prototype.releaseCreatedActivity = function (artist, release) {
   var activity = getstream.createActivity('created', artist, release);
   var feed = this.getReleaseFeed(artist);

   getstream.addActivity(activity, feed);
};

AC.prototype.getProfileUpdatedMessage = function getProfileUpdatedMessage(updates, artist) {
   var message;

   if(updates.name) {
      message = artist.name + ' changed their name';
   } else {
      message = artist.name + ' updated their profile';
   }

   return message;
};

AC.prototype.transformFeedItem = function (feed, user) {
   var body, message, state, stateId, imageUrl, distance;

   function getDistance() {
      return geolib.getDistance({
         latitude: feed.object.location.lat,
         longitude: feed.object.location.lon
      },{
         latitude: user.location.lat,
         longitude: user.location.lon
      });
   }
   
   switch(feed.object_type) {
      case 'event':
      message = feed.actor.name + ' has an event';

      if (user && 
            feed.object.location && feed.object.location.lat && feed.object.location.lon &&
            user.location && user.location.lat && user.location.lon) {
         distance = getDistance(); // in meters
         if (distance < 30000)
            message += ' near you';
      }
      
      if (feed.object.start_date) {
         message += ' on ' + feed.object.start_date.toLocaleDateString();
      }

      imageUrl = feed.object.image_link;
      state = 'event.main';
      stateId = feed.object.id;
      break;

      case 'release':
      message = feed.actor.name + ' released a new ' + feed.object.type;
      imageUrl = feed.object.image_link;
      state = 'release.main';
      stateId = feed.object.id;
      break;

      case 'post':
      message = feed.actor.name + ' blog post';
      body = feed.object.text;
      imageUrl = feed.object.image_link;
      state = 'artist.blog';
      stateId = feed.actor.id;
      break;

      case 'profile':
      message = this.getProfileUpdatedMessage(feed.object_data, feed.actor);
      state = 'artist.profile';
      stateId = feed.actor.id;
      break;

      case 'future_events':
      message = feed.actor.name + ' has upcoming events';
      state = 'artist.events';
      stateId = feed.actor.id;
      break;
   }

   feed.message = message;
   feed.body = body;
   feed.image_link = imageUrl;
   feed.state = state;
   feed.state_id = stateId;
   feed.distance = distance;

   return feed;
};

AC.prototype.transformAggregatedFeedItem = function (feed, user) {
   var message, state, stateId, imageUrl, distance;
   var artist = feed.actor,
      objectType = feed.object_type;

   switch(objectType) {
   case 'profile':
      state = 'artist.profile';
      stateId = artist.id;
      message = artist.name + ' updated their profile';
   }

   feed.message = message;
   feed.image_link = imageUrl;
   feed.state = state;
   feed.state_id = stateId;

   return feed;
};

module.exports = new AC();
