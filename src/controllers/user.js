var Controller = require('./_base.js'),
   constants = require('../constants.js'),
   _ = require('underscore'),
   config = require('config'),
   log = require('../logging.js').logger,
   errors = require('../error'),
   UserModel = require('../models/user.js'),
   UserMongo = require('../mongo/user.js'),
   PasswordReset = require('../mongo/password_reset.js'),
   getstream = require('../util/getstream.js');

var defaultOwner = config.default_owner,
   feed_constants = constants.feeds,
   AuthError = errors.AuthError,
   ControllerError = errors.ControllerError;

var UC = function () {
   Controller.call(this, UserMongo, UserModel);
};

UC.prototype = Object.create(Controller.prototype);

UC.prototype.initPasswordReset = function initPasswordReset(user, callback) {
   var i = 0,
      iterations = 16,
      token = '';

   for (i; i < iterations; i++) {
      // Append HEX representation of masked date integer
      // Creates a 32 character long token
      token += _.random(0, 255).toString(16);
   }

   // Create the doc if it doesn't exist, otherwise just update with new token
   PasswordReset
      .findOneAndUpdate({
         user_id: user.id
      }, {
         token: token,
         created: Date.now()
      }, {
         upsert: true
      })
      .exec();

   return token;
};

UC.prototype.resetPassword = function resetPassword(token, user, password, callback) {
   function failed() {
      callback('The token is incorrect');
   }

   if (!user) return failed();

   PasswordReset.findOne({
      user_id: user.id
   }).exec(function (err, reset) {
      if (err) return callback(err);
      if (!reset) return failed();

      if (token !== reset.token) return failed();

      user.password = password;
      user.save();
      reset.remove();

      callback(null, user);
   });
};

UC.prototype.parseDocument = function parseDocument(doc, callback) {
   var _this = this;
   if (typeof doc === 'string') _this.getByIdOr('email', doc, callback);
   else if (doc.id) _this.getById(doc.id, callback);
   else if (doc.email) _this.getBy('email', doc.email, callback);
   else callback(new ControllerError('There is nothing here'));
};

UC.prototype.deleteDocument = function deleteDocument(user, callback) {
   function transferOwner(obj) {
      if (obj.admins && obj.admins.length > 0) obj.owner = obj.admins.$pop();
      else obj.owner = defaultOwner;

      obj.save();
   }

   user.populate('owned_artists owned_events', function (err, pUser) {
      if (err) return log.error('Failed owner transfer', {
         'artists': user.owned_artists,
         'events': user.owned_events
      });

      pUser.owned_artists.forEach(transferOwner);
      pUser.owned_events.forEach(transferOwner);
      pUser.owned_releases.forEach(transferOwner);
   });

   Controller.prototype.deleteDocument.call(this, user, callback);
};

UC.prototype.validate = function validate(doc, model, user, callback) {
   var _this = this,
      oldPassword = model.password,
      oldPassMatches = true;

   function verifyUser() {
      model = _this.verifyFields(doc, model);
      model.validate(function (err) {
         if (!err && oldPassMatches) return callback();

         var val_errors = err && err.errors || {};

         // switch back new password error
         if (val_errors.password) {
            val_errors.newpassword = val_errors.password;
            delete val_errors.password;
         }

         if (!oldPassMatches) {
            val_errors.password = {
               message: 'Current password incorrect'
            };
         }

         return callback({
            errors: val_errors
         });
      });
   }

   if (!oldPassword) {
      // Validating a new user
      Controller.prototype.validate.call(_this, doc, model, user, callback);
   } else if (doc.newpassword) {
      model.comparePassword(doc.password, function (err, success) {
         if (!success || err) oldPassMatches = false;

         // Let's switch set the new password for validation/saving
         doc.password = doc.newpassword;
         verifyUser();
      });
   } else {
      // Password isn't being changed, so let's wipe it
      delete doc.password;
      verifyUser();
   }
};

//var testEmailRegex = /\d{3}@t.test/;
UC.prototype.postNew = function postNew(doc, callback) {
   log.info('Creating user', doc);

   // Make sure email has no whitespace around it
   doc.email = doc.email.trim();

   // Open up the flood gates
   return Controller.prototype.postNew.call(this, doc, callback);
};

UC.prototype.getNotificationFeed = function(user) {
   return getstream.getFeed(feed_constants.user.notifications, user.id);
};

UC.prototype.getAggregateFeed = function(user) {
   return getstream.getFeed(feed_constants.user.aggregated, user.id);
};

module.exports = new UC();
