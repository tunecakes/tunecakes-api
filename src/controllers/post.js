var Controller = require('./_base.js'),
   PostModel = require('../models/post.js'),
   PostMongo = require('../mongo/post.js');

var PC = function () {
   Controller.call(this, PostMongo, PostModel);
};

PC.prototype = Object.create(Controller.prototype);

module.exports = new PC();
