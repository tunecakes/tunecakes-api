module.exports = {
   artist: require('./artist'),
   event: require('./event'),
   release: require('./release'),
   song: require('./song'),
   user: require('./user')
};
