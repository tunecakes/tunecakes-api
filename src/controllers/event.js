var async = require('async'),
   geolib = require('geolib'),
   Controller = require('./_base.js'),
   ArtistController = require('./artist.js'),
   UserController = require('./user.js'),
   EventModel = require('../models/event.js'),
   EventMongo = require('../mongo/event.js'),
   errors = require('../error'),
   log = require('../logging.js').logger,
   getstream = require('../util/getstream.js'),
   feed_constants = require('../constants').feeds,
   ControllerError = errors.ControllerError;

var EC = function () {
   Controller.call(this, EventMongo, EventModel);
};

EC.prototype = Object.create(Controller.prototype);

EC.prototype.postNew = function postNew(doc, callback) {
   var _this = this;

   Controller.prototype.postNew.call(_this, doc, function(err, event) {
      if(err) return callback(err);

      var feeds = _this.getFlatFeeds(event);

      getstream.followMany(_this.getEverythingFeed(event), feeds)
      .catch(function(err) {
         log.warn('Failed to init event ' + event.id + ' everything feed', err.detail);
      });

      return callback(err, event);
   });
};

EC.prototype.updateDocument = function updateDocument(doc, event, callback) {
   var _this = this;

   Controller.prototype.updateDocument.call(_this, doc, event, function(err, event) {
      if(err) return callback(err);

      _this.eventUpdateActivity(doc, event);
      return callback(err, event);
   });
};

EC.prototype.getFlatFeeds = function getFlatFeeds(event) {
   return [
      this.getAnnouncementFeed(event),
      this.getReminderFeed(event),
      this.getTicketFeed(event),
      this.getOtherFeed(event)
   ];
};

EC.prototype.parseDocument = function parseDocument(doc, callback) {
   if (typeof doc === 'string') this.getById(doc, callback);
   else if (doc.id) this.getById(doc.id, callback);
   else callback(new ControllerError('There is nothing here'));
};

EC.prototype.saveDocument = function saveDocument(doc, model, callback) {
   var _this = this;

   async.parallel([
      function (cb) {
         if (!doc.owner) return cb();

         UserController.parseDocument(doc.owner, function (err, userMongo) {
            doc.owner = userMongo.id;
            userMongo.owned_events.addToSet(model);
            userMongo.save();
            cb();
         });
      },
      function (cb) {
         UserController.parseDocumentArray(doc.admins, function (admin) {
            admin.admin_events.addToSet(model);
            admin.save();
         }, function (err, failedAdmin) {}, function (admins, failedAdmins) {
            cb();
         });
      },
      function (cb) {
         // this needs to be doc.artists because we haven't
         // parsed out to linked_artists yet
         ArtistController.parseDocumentArray(doc.artists, function (artist) {
            ArtistController.addEvent(artist, model);
         }, function () {}, function (artists, failedArtists) {
            cb();
         });

      }
   ], function () {
      Controller.prototype.saveDocument.call(_this, doc, model, callback);
   });
};

EC.prototype.sendTimeNotification = function sendTimeNotification(users, event, updateMsg, callback) {
   var activity, feeds = [];

   async.forEach(users, function (user) {
      feeds.push(getstream.getFeed(feed_constants.user.notifications, user.id));
   });

   activity = getstream.createActivity();

   getstream.addActivityToManyFeeds(activity, feeds, callback);
};

EC.prototype.addActivitiesOnCreate = function addActivitiesOnCreate(event, user) {
   if (event.linked_artists && event.linked_artists.length > 0) {
      event.populate('linked_artists', function (err, pEvent) {
         if (err) log.error('Error occurred populating artists while adding activities for event ' + event.id);
         async.forEach(pEvent.linked_artists, function (artist) {
            ArtistController.addActivityToFeed(artist, event, 'added', [feed_constants.artist.everything, feed_constants.artist.events]);
         });
      });
   }
};

EC.prototype.getEverythingFeed = function(event) {
   return getstream.getFeed(feed_constants.event.everything, event.id);
};

EC.prototype.getReminderFeed = function(event) {
   return getstream.getFeed(feed_constants.event.reminder, event.id);
};

EC.prototype.getAnnouncementFeed = function(event) {
   return getstream.getFeed(feed_constants.event.announcements, event.id);
};

EC.prototype.getTicketFeed = function(event) {
   return getstream.getFeed(feed_constants.event.tickets, event.id);
};

EC.prototype.getOtherFeed = function(event) {
   return getstream.getFeed(feed_constants.event.other, event.id);
};

EC.prototype.eventUpdateActivity = function(updates, event) {
   var activity = getstream.createActivity('updated', event, updates, 'SELF');
   var feed = this.getOtherFeed(event);

   getstream.addActivity(activity, feed);
};

EC.prototype.transformFeedItem = function (feed, user) {
   var body, distance, message='Event updated';

   function getDistance() {
      return geolib.getDistance({
         latitude: feed.actor.location.lat,
         longitude: feed.actor.location.lon
      },{
         latitude: user.location.lat,
         longitude: user.location.lon
      });
   }

   if (user && user.location && user.location.lat && user.location.lon &&
       feed.actor.location && feed.actor.location.lat && feed.actor.location.lon) {

      distance = getDistance(); // in meters
      if (distance < 30000) {
         message += ' near you';
      }
   }

   feed.message = message;
   feed.body = body;
   feed.image_link = feed.actor.image_link;
   feed.state = 'event.main';
   feed.state_id = feed.actor.id;
   feed.distance = distance;
};

EC.prototype.transformAggregatedFeedItem = function (feed, user) {
   var event = feed.activities[0].actor;

   feed.message = 'Event updated';
   feed.image_link = event.image_link;
   feed.state = 'event.main';
   feed.state_id = event.id;
};

module.exports = new EC();
