var RequestModel = require('../models/request.js'),
   RequestMongo = require('../mongo/request.js'),
   Controller = require('./_base');

var RC = function () {
   Controller.call(this, RequestMongo, RequestModel);
};

RC.prototype = Object.create(Controller.prototype);

module.exports = new RC();
