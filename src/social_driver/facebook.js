var config =		require('config');
var APP_ID =		config.get("development.facebook.clientID");
var APP_SECRET = 	'8b21c08455bae3703add0a84e40e9ffa';
var HOST = 			'graph.facebook.com';
var qs = 			require('querystring');
var https = 			require('https');
var log = require('../logging.js').logger;

exports.postPageStatusUpdate = function(data, callback){

   var postData = qs.stringify({
      'message':		data.message,
      'page_token':	data.pageToken
   });

   var options = {
      host:	HOST,
      port:	443,
      path:	'/' + data.pageID + '/feed',
      method:	'POST',
      headers: {
	 'Content-Type': 'application/x-www-form-urlencoded',
	 'Content-Length': postData.length
      }
   };

   var request = https.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function(chunk){
	 log.info('Facebook Post Response: ' + chunk);
      });
   });

   request.write(postData);
   request.end();
   callback();
};

exports.postUserStatusUpdate = function(data, callback){

   var postData = qs.stringify({
      'message':		data.message,
      'access_token':	data.userToken
   });

   var options = {
      host:	HOST,
      port:	443,
      path:	'/' + data.userID + '/feed',
      method:	'POST',
      headers: {
	 'Content-Type': 'application/x-www-form-urlencoded',
	 'Content-Length': postData.length
      }
   };

   var request = https.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function(chunk){
	 log.info('Facebook Post Response: ' + chunk);
      });
   });

   request.write(postData);
   request.end();
   callback();
};
