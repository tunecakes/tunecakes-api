var Schema = require('mongoose').Schema;

var feedItem = {
   name: 'FeedItem',
   mongoose: {
      feed: {required: true, type: String, trim: true},
      actor: Schema.ObjectId,
      actor_type: {required: true, type: String, trim: true},
      object: Schema.Types.Mixed,
      object_type: {required: true, type: String, trim: true},
      object_data: {},
      verb: {required: true, type: String, trim: true},
      time: Date,
      getstream_id: String
   }
};

module.exports = feedItem;
