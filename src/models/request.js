var Schema = require('mongoose').Schema;

var request = {
   name: 'request',
   mongoose: {
      artist_name: String,
      created: Date,
      user_origin: {
         type: Schema.ObjectId,
         ref: 'user'
      },
      spotify_id: String,
      spotify_url: String,
      facebook_id: String,
      facebook_url: String,
      twitter_id: String,
      twitter_url: String,
      official_site_url: String,
   },
   permissions: {
      user: {},
      admin: {},
      owner: {},
   }
};

module.exports = request;
