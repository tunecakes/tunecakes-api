var Schema = require('mongoose').Schema;

var event = {
   name: 'event',
   populate: 'linked_artists',
   mongoose: {
      // Standard stuff
      created: Date,
      updated: Date,
      admins: [{
         type: Schema.ObjectId,
         ref: 'user'
      }],
      owner: {
         type: Schema.ObjectId,
         ref: 'user'
      },

      type: {
         type: String,
         enum: ['Concert', 'Festival', 'Secret Show', 'Studio'],
         default: 'Concert'
      },

      name: {
         type: String
      },
      description: String,
      tags: {
         type: [String],
         trim: true
      },
      linked_artists: [{
         type: Schema.ObjectId,
         ref: 'artist'
      }],
      posts: [{
         type: Schema.ObjectId,
         ref: 'post'
      }],
      start_date: {
         type: Date,
         required: true
      },
      end_date: {
         type: Date
      },

      door_price: String,
      early_price: String,
      currency: String,
      purchase_url: String,
      purchase_notes: String,

      image_link: String,
      audio_link: String,
      video_link: String,

      ticket_url: String,
      ticket_info: String,
      venue_name: String,
      venue_url: String,

      reminder: {
         type: Schema.ObjectId,
         ref: 'reminder'
      },

      unlinked_artists: [Schema.Types.Mixed],
      external_url: String
   },
   mongoose_indexes: [{
      start_date: 1
   }],
   permissions: {
      user: {
         owner: 'X',
         admin: 'X',
      },

      admin: {
         owner: 'R',
         admin: 'R',
         created: 'R',
         updated: 'R',
      },

      owner: {
         created: 'R',
         updated: 'R',
      }
   }
};

module.exports = event;
