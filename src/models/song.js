var Schema = require('mongoose').Schema;

var song = {
   name: 'song',
   populate: [{
      path: 'artist',
      select: 'name'
   }],
   mongoose: {
      created: Date,
      updated: Date,
      track_number: String,
      name: {
         type: String,
         required: true
      },
      description: String,
      tags: [{
         type: String,
         trim: true
      }],
      image_link: String,
      audio_link: String,
      video_link: String,
      artist: {
         type: Schema.ObjectId,
         ref: 'artist'
      },
      contributors: [{
         role: String,
         artist: {
            type: Schema.ObjectId,
            ref: 'artist'
         }
      }],
      published: {
         type: Date
      },
      length: Number,
      isrc: String,
      mbid: String
   },
   elastic: {
      name: 'store',
      tags: 'store',
      published: 'store',
      linked_artists: {
         name: {
            store: true,
            type: 'string'
         }
      }
   }
};

module.exports = song;
