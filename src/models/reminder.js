var reminder = {
   created: Date,
   updated: Date,
   remind_date: Date,
   entity_id: String,
   entity_id_type: String,
   email_addresses: [String],
   feed_flags: [String],
   sent: Boolean,
   data: {}
};

module.exports = reminder;
