var Schema = require('mongoose').Schema;

var user = {
   name: 'user',
   mongoose: {
      // Standard stuff
      created: Date,
      updated: Date,

      // User information
      first_name: {
         type: String,
         trim: true
      },
      last_name: {
         type: String,
         trim: true
      },
      birth_date: {
         type: Date
      },
      email: {
         unique: true,
         type: String,
         trim: true,
         required: true
      },
      password: {
         type: String,
         required: true
      },
      default_view: {
         type: String,
         trim: true,
         default: 'fan',
         required: true
      },

      // Artist Subdocuments
      owned_artists: [{
         type: Schema.ObjectId,
         ref: 'artist'
      }],
      admin_artists: [{
         type: Schema.ObjectId,
         ref: 'artist'
      }],

      // Event Subdocuments
      owned_events: [{
         type: Schema.ObjectId,
         ref: 'event'
      }],
      admin_events: [{
         type: Schema.ObjectId,
         ref: 'event'
      }],

      // Event Subdocuments
      owned_releases: [{
         type: Schema.ObjectId,
         ref: 'release'
      }],
      admin_releases: [{
         type: Schema.ObjectId,
         ref: 'release'
      }],

      facebook: {},
      spotify: {},
      twitter: {},
      pending_messages: [],
      tours: {}
   },
   permissions: {
      user: {
         created: 'X',
         updated: 'X',
         first_name: 'X',
         last_name: 'X',
         birth_date: 'X',
         zip: 'X',
         email: 'X',
         password: 'X',
         default_view: 'X',
         is_artist: 'X',
         is_venue: 'X',
         owned_artists: 'X',
         admin_artists: 'X',
         owned_events: 'X',
         admin_events: 'X',
         facebook: 'X',
         twitter: 'X',
         pending_messages: 'X',
         tours: 'X'
      },

      admin: {
         created: 'X',
         updated: 'X',
         first_name: 'X',
         last_name: 'X',
         birth_date: 'X',
         zip: 'X',
         email: 'X',
         password: 'X',
         default_view: 'X',
         is_artist: 'X',
         is_venue: 'X',
         owned_artists: 'X',
         admin_artists: 'X',
         owned_events: 'X',
         admin_events: 'X',
         facebook: 'X',
         twitter: 'X',
         pending_messages: 'X',
         tours: 'X'
      },

      owner: {
         created: 'R',
         updated: 'R',
         is_venue: 'R',
         owned_artists: 'R',
         admin_artists: 'R',
         owned_events: 'R',
         admin_events: 'R',
      }
   }
};

module.exports = user;
