var Schema = require('mongoose').Schema;

var post = {
   name: 'post',
   mongoose: {
      created: Date,
      updated: Date,
      author: {
         type: Schema.ObjectId,
         ref: 'user'
      },

      title: String,
      tweet_text: String,
      text: String,
      image_link: String,
      video_link: String,
      
      entity_id: { type: String, index: true },
      entity_type: String,

      facebook_id: String,
      twitter_id: String,
      instagram_id: String
   }
};

module.exports = post;
