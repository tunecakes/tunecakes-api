var config =		require('config');
var APP_ID =		config.get("development.facebook.clientID");
var APP_SECRET = 	'8b21c08455bae3703add0a84e40e9ffa';
var HOST = 			'graph.facebook.com';
var qs = 			require('querystring');
var https = 			require('https');
var log = require('../logging.js').logger;

exports.postPageStatusUpdate = function(message, options){

   var postData = qs.stringify({
      'message':		message,
      'page_token':	options.pageToken
   });

   var options = {
      host:	HOST,
      port:	443,
      path:	'/' + opt.pageID + '/feed',
      method:	'POST',
      headers: {
	 'Content-Type': 'application/x-www-form-urlencoded',
	 'Content-Length': postData.length
      }
   };

   var request = http.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function(chunk){
	 log.info('Facebook Post Response: ' + chunk);
      });
   });

   request.write(postData);
   request.end();
};

exports.postUserStatusUpdate = function(message, opt){

   var postData = qs.stringify({
      'message':		message,
      'access_token':	opt.userToken
   });

   var options = {
      host:	HOST,
      port:	443,
      path:	'/' + opt.userID + '/feed',
      method:	'POST',
      headers: {
	 'Content-Type': 'application/x-www-form-urlencoded',
	 'Content-Length': postData.length
      }
   };

   var request = https.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function(chunk){
	 log.info('Facebook Post Response: ' + chunk);
      });
   });

   request.write(postData);
   request.end();
};

function postUserStatusUpdate(message, options){
   var postData = qs.stringify({
      'message':		message,
      'access_token':	options.userToken
   });

   var options = {
      host:	HOST,
      port:	80,
      path:	'/' + userID + '/feed',
      method:	'POST',
      headers: {
	 'Content-Type': 'application/x-www-form-urlencoded',
	 'Content-Length': postData.length
      }
   };

   var request = https.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function(chunk){
	 log.info('Facebook Post Response: ' + chunk);
      });
   });

   request.write(postData);
   request.end();
};
