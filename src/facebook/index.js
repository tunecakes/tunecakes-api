var FB = require('fb'),
   config = require('config'),
   request = require('request-promise'),
   qs = require('querystring'),
   clientId = config.facebook.clientID,
   clientSecret = config.facebook.clientSecret;


exports.getExtendedToken = function getExtendedToken(token) {
   var facebookTokenUrl = 'https://graph.facebook.com/oauth/access_token';
   var query = {
      grant_type: 'fb_exchange_token',
      client_id: clientId,
      client_secret: clientSecret,
      fb_exchange_token: token
   };
   console.log('TOKEN BODY', query);

   return request({url: facebookTokenUrl, qs: query})
   .then(function(oauthString) {
      return JSON.parse(oauthString);
   })
   .then(function(oauth) {
      console.log('OAUTH RESPONSE', oauth, oauth.access_token, typeof(oauth))
      return oauth.access_token;
   });
};

exports.getPageToken = function getPageToken(pageId, token) {
   var facebookTokenUrl = 'https://graph.facebook.com/' + pageId;
   var query = {
      client_id: clientId,
      client_secret: clientSecret,
      access_token: token,
      fields: 'access_token'
   };

   console.log('PAGE TOKEN BODY', query);
   return request({url: facebookTokenUrl, qs: query, json: true})
   .then(function(oauth) {
      return oauth.access_token;
   });
};

exports.postToPage = function postToPage(message, token) {
   var fb = FB.withAccessToken(token);

   return new Promise(function(resolve, reject) {
      fb.api('/me/feed', 'POST', {
         message: message
      }, function(response) {
         if(!response) return reject('No response from FB posting to page feed');
         if(response.error) return reject(response.error);
         
         resolve(response);
      });
   });
};
