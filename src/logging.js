'use strict';

var util = require('util'),
   winston = require('winston'),
   config = require('config'),
   logger = new winston.Logger();

logger.add(winston.transports.Console, {
   colorize: true,
   timestamp: true,
   json: false,
   humanReadableUnhandledException: true,
   level: config.log_level
});

//logger.add(winston.transports.File, {
//   filename: config.get("log_directory") + '/application.log',
//   exitOnError: false,
//   level: 'debug'
//});

function formatArgs(args) {
   return util.format(Array.prototype.slice.call(args));
}

module.exports = {
   middleware: function (req, res, next) {
      logger.info('REQUEST', req.method, req.originalUrl);
      logger.debug('REQUEST BODY', req.body)
      logger.debug('REQUEST QUERY', req.query)
      var oldSend = res.send;
      res.send = function () {
         logger.debug('RESPONSE', req.method, req.originalUrl, arguments)
         oldSend.apply(res, arguments);
      };
      next();
   },
   logger: logger
};
