var config = require('config'),
   log = require('../logging.js').logger,
   mailer = require('nodemailer');


var smtpTransport = mailer.createTransport({
   host: config.get("emailer.invites.host"),
   port: config.get("emailer.invites.port"),
   auth: {
      user: config.get("emailer.invites.email"),
      pass: config.get("emailer.invites.password")
   }
});

var sendPasswordReset = function (token, email) {
   var mail = {
      from: "TuneCakes Invites <" + config.get("emailer.invites.email") + ">",
      to: email,
      subject: "Tunecakes Account Password Reset"
   };

   mail["text"] = "Go to " + config.get("web.address") + "/password_reset/" +
      email + "?token=" + token;

   smtpTransport.sendMail(mail, function (error, info) {
      if (error) {
         log.error(error);
      } else {
         log.info("Message sent: " + info.response);
      }
   });

};

module.exports = {
   sendPasswordReset
};
