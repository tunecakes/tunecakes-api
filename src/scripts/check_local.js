var fs = require('fs');
var path = require('path');

var local_config_file_name = 'config/local-development.js';
var missing = [];
var local_config;

var required_locals = [
   'getstream.clientID',
   'getstream.clientKey',
   'getstream.clientSecret'
];

try {
   local_config = require(path.join(__dirname, '..', '..', local_config_file_name));

   for (var i = 0; i < required_locals.length; i++) {
      if (getPropByString(local_config, required_locals[i]) == undefined)
         missing.push(required_locals[i]);
   }
} catch (e) {
   missing = required_locals;
   local_config = {};
}

if (missing.length) {
   var prompt = require('prompt');
   prompt.start();
   prompt.get(missing, function (err, result) {
      if (err) return console.error(err);
      for (var key in result) {
         setPropByString(local_config, key, result[key]);
      }
      var file_contents = 'module.exports = ' + JSON.stringify(local_config, null, 3);
      fs.writeFileSync(local_config_file_name, file_contents, 'utf8');
   });
}

function getPropByString(obj, propString) {
   if (!propString)
      return obj;

   var prop, props = propString.split('.');

   for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
      prop = props[i];

      var candidate = obj[prop];
      if (candidate != undefined) {
         obj = candidate;
      } else {
         break;
      }
   }
   return obj[props[i]];
}

function setPropByString(obj, propString, value) {
   if (!propString)
      return obj;

   var prop;
   var props = propString.split('.');

   for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
      prop = props[i];

      var candidate = obj[prop];
      if (candidate !== undefined) {
         obj = candidate;
      } else {
         obj[prop] = {};
         obj = obj[prop];
      }
   }

   obj[props[i]] = value;
}
