var config = require('config'),
   prompt = require('prompt'),
   elasticSearch = require('elasticsearch');

var conf = config.get('elastic.connection');

var client = new elasticSearch.Client(conf);

var reindexInfo = [
   'source_host',
   'source_index',
   'source_type',
   'dest_type'
];

prompt.start();
prompt.get(reindexInfo, (err, results) => {
   client.reindex({
      body: {
         source: {
            index: results.source_index,
            type: results.source_type,
            remote: {
               host: results.source_host
            }
         },
         dest: {
            index: results.dest_index
         }
      }
   })
   .then(() => {
      console.log('Successfully reindexed elastic', results);
   })
   .catch((err) => {
      console.log('Failed to reindex elastic', results, err);
   });
});
