var config = require('config'),
   elasticSearch = require('elasticsearch'),
   artist = require('../elastic/artist.js'),
   event = require('../elastic/event.js');

var conf = config.get('elastic.connection');

var client = new elasticSearch.Client(conf);

function createArtistIndex() {
   return client.indices.create({
      index: artist.index,
      body: artist.settings
   })
   .then(function() {
      console.log('Successfully created artist index', artist.index);
   })
   .catch(function(err) {
      console.error('Could not create artist index', artist.index, err);
      throw err;
   });
}

function createEventIndex() {
   return client.indices.create({
      index: event.index,
      body: event.settings
   })
   .then(function() {
      console.log('Successfully created event index', event.index);
   })
   .catch(function(err) {
      console.error('Could not create event index', event.index, err);
      throw err;
   });
}

function createArtistMapping() {
   return client.indices.putMapping({
      index: artist.index,
      type: artist.index,
      body: {properties: artist.properties}
   })
   .then(function() {
      console.log('Successfully created artist mapping');
   })
   .catch(function(err) {
      console.error('Could not create artist mapping', err);
      throw err;
   });
}

function createEventMapping() {
   return client.indices.putMapping({
      index: event.index,
      type: event.index,
      body: {properties: event.properties}
   })
   .then(function() {
      console.log('Successfully created event mapping');
   })
   .catch(function(err) {
      console.error('Could not create event mapping', err);
      throw err;
   });
}

createArtistIndex()
.then(createArtistMapping);

createEventIndex()
.then(createEventMapping);
