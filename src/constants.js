module.exports = {
   permissions: {
      NONE: 'X',
      READ_ONLY: 'R',
      READ_WRITE: 'RW',
      roles: {
         USER: 'user',
         ADMIN: 'admin',
         OWNER: 'owner'
      }
   },
   feeds: {
      artist: {
         everything: 'artistEverything',
         events: 'artistEvents',
         blog: 'artistBlog',
         other: 'artistOther',
         releases: 'artistReleases',
      },
      event: {
         announcements: 'eventAnnouncements',
         reminder: 'eventReminder',
         tickets: 'eventTickets',
         everything: 'eventEverything',
         other: 'eventOther'
      },
      user: {
         notifications: 'userNotifications',
         aggregated: 'userAggregated',
      },
   },
   NO_USER: 'ANONYMOUS USER',
   ZIP_REGEX: /^(\d{5})?$/,
   EMAIL_REGEX: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
   ALIAS_REGEX: /^[a-z0-9]+(_?[a-z0-9]+)*$/,
   TAGS_REGEX: /^[a-z0-9](.?[a-z0-9])*$/
};
