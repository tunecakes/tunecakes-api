var mongoose = require('mongoose');
var chai = require('chai');
var chaiSubset = require('chai-subset');
var expect = chai.expect;
var util = require('./test_util');
var POST = util.post;

// Set up Chai
chai.use(chaiSubset);

/*
 * Synchronous test dependencies
 */

// clear database
before(function (done) {
   mongoose.connection.on('open', function () {
      mongoose.connection.db.dropDatabase(function () {
         done();
      });
   });
});