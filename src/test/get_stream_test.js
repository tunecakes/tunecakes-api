var config = require('config'),
   stream = require('getstream'),
   feed_constants = require('../constants.js').feeds,
   client = stream.connect(
      config.get("getstream.clientKey"),
      config.get("getstream.clientSecret"),
      config.get("getstream.clientID")
   );

var a = client.feed(feed_constants.user.aggregated, '5798162290a650fc0f94b26b');

a.follow(feed_constants.artist.everything, '57748579f545cbd8098d1ab5')
.then(function(done) {
   console.log('Success', done);
})
.catch(function(err) {
   console.log('Fail', err.error);
      
});
