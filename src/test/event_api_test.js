var chai = require('chai'),
   expect = chai.expect,
   util = require('./test_util'),
   api = util.api,
   userUtil = util.user,
   artistUtil = util.artist;

var endpoint = '/api/event/';
var user1, user2, user3, user4, user5;
var artist1, artist2, artist3, artist4, artist5;
var event1, event2, event3, event4, event5;

// Create users
before(function (done) {
   userUtil.newUser('eventUser1@t.test', function(usr) {
      user1 = usr;
      userUtil.newUser('eventUser2@t.test', function(usr) {
         user2 = usr;
         userUtil.newUser('eventUser3@t.test', function(usr) {
            user3 = usr;
            userUtil.newUser('eventUser4@t.test', function(usr) {
               user4 = usr;
               userUtil.newUser('eventUser5@t.test', function(usr) {
                  user5 = usr;
                  done();
               });
            });
         });
      });
   });
});

// Create Artists
before(function(done) {
   artistUtil.newArtist(user1, 'eventArtist1', function(art) {
      artist1 = art;
      artistUtil.newArtist(user1, 'eventArtist2', function(art) {
         artist2 = art;
         artistUtil.newArtist(user1, 'eventArtist3', function(art) {
            artist3 = art;
            artistUtil.newArtist(user1, 'eventArtist4', function(art) {
               artist4 = art;
               artistUtil.newArtist(user1, 'eventArtist5', function(art) {
                  artist5 = art;
                  done();
               });
            });
         });
      });
   });
});

// Set event data
before(function(done) {
   event1 = {
      name: 'Event Test 1',
      artists: [artist1.id],
      zip: '23443'
   };

   event2 = {
      name: 'Event Test 2',
      artists: [artist1.id, artist2.id],
      zip: '00123'
   };

   done();
});

describe('Event Endpoint', function () {
   this.timeout(5000);

   describe('POST', function () {
      it('should fail without any auth token', function (done) {
         api.post(endpoint)
            .expect(function (res) {
               expect(res.body.errors).to.not.be.null;
            })
            .expect(401, done);
      });

      it('should create an event successfully', function (done) {
         api.post(endpoint)
            .set('Authorization', userUtil.authToken(user1))
            .send(event1)
            .expect(200)
            .end(function(err, res) {
               expect(res.body.href).to.equal(endpoint + res.body.id);
               expect(res.body.artists).to.have.length(1);

               event1 = res.body;
               done();
            });
      });

      it('should validate properties', function (done) {
         //TODO: fill this out once we have what needs to be fixed
         done();
      });
   });

   describe('GET', function () {
      it('should successfully retrieve array of events', function (done) {
         api.get(endpoint)
            .expect(function (res) {
               expect(res.body).to.be.an('array');
               expect(res.body.length).to.equal(1);
               expect(res.body[0].id).to.equal(event1.id);

               event1 = res.body[0]
            })
            .expect(200, done);
      });

      it('should be able to get a specific event from its id', function (done) {
         api.get(event1.href)
             .expect(function (res) {
                expect(res.body).to.deep.equal(event1);
             })
             .expect(200, done);
      });
   });

   describe('PUT', function() {
      it('should fail without valid auth token', function (done) {
         api.put(event1.href)
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should successfully update event name', function (done) {
         event1.name = 'changed';
         api.put(event1.href)
             .set('Authorization', userUtil.authToken(user1))
             .send(event1)
             .expect(function (res) {
                expect(res.body.name).to.equal(event1.name);
             })
             .expect(200, done);
      });

      it('should fail to update if user not admin', function (done) {
         api.put(event1.href)
            .set('Authorization', userUtil.authToken(user2))
            .send(event1)
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });
   });

   describe('DELETE', function() {
      it('should fail without valid auth token', function (done) {
         api.del(event1.href)
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should fail if user not admin', function (done) {
         api.del(event1.href)
            .set('Authorization', userUtil.authToken(user2))
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should successfully delete event', function (done) {
         api.del(event1.href)
             .set('Authorization', userUtil.authToken(user1))
             .expect(function (res) {
                expect(res.body.name).to.equal(event1.name);
             })
             .expect(200)
             .end(function () {
                api.get(event1.href)
                    .expect(404, done);
             });
      });

   })
});
