var request = require('supertest'),
   app = require('../../app');

/*
 * Generic request helpers
 *  always make sure that the content type is JSON
 */

function get(path) {
   return request(app).get(path).expect('content-type', /json/);
}

function post(path) {
   return request(app).post(path).expect('content-type', /json/);
}

function put(path) {
   return request(app).put(path).expect('content-type', /json/);
}

function del(path) {
   return request(app).delete(path).expect('content-type', /json/);
}

module.exports = {
   get: get,
   post: post,
   put: put,
   del: del
};
