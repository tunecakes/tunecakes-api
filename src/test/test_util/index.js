module.exports = {
   api: require('./api'),
   user: require('./user'),
   artist: require('./artist')
};