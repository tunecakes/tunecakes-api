var config = require('config'),
   chai = require('chai'),
   expect = chai.expect,
   api = require('./api');

// Keys are user emails. Auth tokens and passwords are stored in the map
var userMap = {};

function newUser(email, callback) {
   // Might want to randomly gen this for each user
   var password = 'aA!bB09=/;"';

   // Verify email is unique before continuing;
   expect(userMap[email], email + ' already exists.').to.be.undefined;

   // Add email to allowed list
   config.allowed_emails.push(email);

   // Create user
   api.post('/api/user').send({email: email, password: password}).expect(200).end(function (err, res) {
      api.post('/api/authenticate').send({
         email: email,
         password: password
      }).expect(200).end(function (err, data) {
         expect(data.body.token).to.not.be.null;

         // Store this user in the userMap
         userMap[email] = {
            password: password,
            authToken: 'Bearer ' + data.body.token
         }

         callback(res.body);
      });
   });
}

function refreshUser(user, callback) {
   // Verify user exists
   expect(userMap[user.email], user.email + ' should exist.').to.not.be.undefined;

   // Get updated user data
   api.get('/api/user/' + user.email)
      .set('Authorization', userMap[user.email].authToken)
      .expect(200)
      .end(function(err, userData) {
         callback(userData);
      });
}

function authToken(user) {
   // Verify user exists
   expect(userMap[user.email], user.email + ' should exist.').to.not.be.undefined;

   return userMap[user.email].authToken;
}

function fakeAuthToken() {
   return 'This Token Is Fake As Fuck';
}

module.exports = {
   newUser:  newUser,
   refreshUser: refreshUser,
   authToken: authToken,
   fakeAuthToken: fakeAuthToken
};