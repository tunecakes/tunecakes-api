var config = require('config'),
   chai = require('chai'),
   expect = chai.expect,
   api = require('./api'),
   userUtil = require('./user');

// Maps artist alias to owner
var artistMap = {};

function newArtist(user, alias, callback) {
   // Verify alias is unique before continuing;
   expect(artistMap[alias], alias + ' already exists.').to.be.undefined;

   api.post('/api/artist').send({name: alias, alias: alias})
      .set('Authorization', userUtil.authToken(user))
      .expect(200)
      .end(function(err, res) {
         artistMap[alias] = user;
         callback(res.body);
      });
}

function refreshArtist(artist, callback) {
   // Verify alias exists before continuing;
   expect(artistMap[artist.alias], artist.alias + ' should exist.').to.not.be.undefined;

   api.get('/api/artist/' + artist.alias)
      .set('Authorization', userUtil.authToken(artistMap[artist.alias]))
      .expect(200)
      .end(function(err, res) {
         callback(res.body);
      });
}

module.exports = {
   newArtist: newArtist,
   refreshArtist: refreshArtist
}