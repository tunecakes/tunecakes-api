// Set up environment
process.env.NODE_ENV = 'test';

var Mocha = require('mocha'),
    mocha = new Mocha(),
    path = require('path'),
    thisDir = __dirname,
    testInitFile = 'test_init.js';


var integrationTestFiles = {
   artist: 'artist_api_test.js',
   event: 'event_api_test.js',
   release: 'release_api_test.js'
};

function addMochaFile(filePath) {
   console.log('Adding ' + filePath);
   var fullPath = path.join(thisDir, filePath);
   mocha.addFile(fullPath);
}

// Initialize mocha tests. test_util.js contains the request methods
var app = require('./test_util');

// Add other test files to mocha
console.log('\nAdding files to mocha\n===================');
addMochaFile(testInitFile);
if(process.argv.length === 2) {
   for(file in integrationTestFiles) addMochaFile(integrationTestFiles[file]);
} else {
   var testToRun = process.argv[2].toLowerCase();
   if(integrationTestFiles[testToRun]) addMochaFile(integrationTestFiles[testToRun]);
   else {
      console.log(testToRun + ' is not a test. Please do better. Fuck you. <3 BS');
      process.exit(-1);
   }
}

// Log exceptions to terminal 
//process.on('uncaughtException', function(err) {
//   console.log(err.stack);
//})

// Run mocha tests
console.log('\nRunning Mocha Tests...');
mocha.run(function(failures){
   process.on('exit', function () {
      process.exit(failures);
   });

}).on('end', function() {
   process.exit();
});
