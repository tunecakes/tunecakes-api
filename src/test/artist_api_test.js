var chai = require('chai'),
   expect = chai.expect,
   util = require('./test_util'),
   api = util.api,
   userUtil = util.user;

var endpoint = '/api/artist/';
var user1, user2, user3, user4, user5;
var artist1, artist2, artist3, artist4, artist5;

// Create users
before(function (done) {
   userUtil.newUser('artistUser1@t.test', function(usr) {
      user1 = usr;
      userUtil.newUser('artistUser2@t.test', function(usr) {
         user2 = usr;
         userUtil.newUser('artistUser3@t.test', function(usr) {
            user3 = usr;
            userUtil.newUser('artistUser4@t.test', function(usr) {
               user4 = usr;
               userUtil.newUser('artistUser5@t.test', function(usr) {
                  user5 = usr;
                  done();
               });
            });
         });
      });
   });
});

// Set artist data
before(function(done) {
   artist1 = {
      name: 'ME',
      zip: '27606',
      alias: 'poop'
   };

   artist2 = {
      name: 'ME 2!',
      zip: '07606',
   };

   done();
});

describe('Artist Endpoint', function () {
   this.timeout(5000);

   describe('POST', function () {
      /*
       * Token validation
       */
      it('should fail without any auth token', function (done) {
         api.post(endpoint)
            .expect(function (res) {
               expect(res.body.errors).to.not.be.null;
            })
            .expect(401, done);
      });

      it('should fail without a valid auth token', function (done) {
         api.post(endpoint)
            .set('Authorization', userUtil.fakeAuthToken())
            .expect(function (res) {
               expect(res.body.errors).to.not.be.null;
            })
            .expect(401, done);
      });

      it('should create an artist with an alias', function (done) {
         api.post(endpoint)
            .set('Authorization', userUtil.authToken(user1))
            .send(artist1)
            .expect(function(res) {
               expect(res.body).to.containSubset(artist1);
               // Make sure href is correctly set
               expect(res.body.href).to.equal(endpoint + res.body.alias);
               artist1 = res.body;
            })
            .expect(200, done);
      });

      it('should fail to create an artist with an alias that\'s already used', function (done) {
         api.post(endpoint)
            .set('Authorization', userUtil.authToken(user2))
            .send(artist1)
            .expect(400, done);
      });

      it('should create an artist with no alias', function (done) {
         api.post(endpoint)
            .set('Authorization', userUtil.authToken(user1))
            .send(artist2)
            .expect(function(res) {
               expect(res.body).to.containSubset(artist2);
               // Make sure href is correctly set
               expect(res.body.href).to.equal(endpoint + res.body.id);
               artist2 = res.body;
            })
            .expect(200, done);
      });

      it('should validate properties', function (done) {
         //TODO: fill this out once we have what needs to be fixed
         done();
      });
   });

   describe('GET', function () {
      it('should successfully retrieve array of artists', function (done) {
         api.get(endpoint)
            .expect(function (res) {
               expect(res.body).to.be.an('array');
               expect(res.body.length).to.be.gt(0);
            })
            .expect(200, done);
      });

      it('should be able to GET a specific artist from its alias', function (done) {
         api.get(artist1.href)
            .expect(function (res) {
               expect(res.body).to.deep.equal(artist1);
               expect(artist1.href).to.equal(endpoint + artist1.alias);
            })
            .expect(200, done);
      });

      it('should be able to GET a specific artist from its id', function (done) {
         api.get(artist2.href)
            .expect(function (res) {
               expect(res.body).to.deep.equal(artist2);
               expect(artist2.href).to.equal(endpoint + artist2.id);
            })
            .expect(200, done);
      });
   });

   describe('PUT', function() {
      it('should fail without valid auth token', function (done) {
         api.put(artist1.href)
            .set('Authorization', userUtil.fakeAuthToken())
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should successfully update artist with alias', function (done) {
         artist1.name = 'changed';
         api.put(artist1.href)
            .set('Authorization', userUtil.authToken(user1))
            .send(artist1)
            .expect(function (res) {
               // Test and save updated date
               expect(artist1.updated).to.be.lt(res.body.updated);
               artist1.updated = res.body.updated;

               // Test other data
               expect(res.body).to.deep.equal(artist1);
            })
            .expect(200, done);
      });

      it('should successfully update artist with sparse changes', function (done) {
         var changes = {description: 'NOW I CAN FINALLY DESCRIBE MYSELF'};
         api.put(artist1.href)
            .set('Authorization', userUtil.authToken(user1))
            .send(changes)
            .expect(function (res) {
               expect(res.body).to.containSubset(changes);
               artist1 = res.body;
            })
            .expect(200, done);
      });

      it('should successfully update artist with ID and no alias', function (done) {
         artist2.name = 'changed';
         api.put(artist2.href)
            .set('Authorization', userUtil.authToken(user1))
            .send(artist2)
            .expect(function (res) {
               // Test and save updated date
               expect(artist2.updated).to.be.lt(res.body.updated);
               artist2.updated = res.body.updated;

               // Test other data
               expect(res.body).to.deep.equal(artist2);
            })
            .expect(200, done);
      });

      it('should fail to update if user not admin', function (done) {
         api.put(artist1.href)
            .set('Authorization', userUtil.authToken(user5))
            .send(artist1)
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });
   });

   describe('DELETE', function() {
      it('should fail without valid auth token', function (done) {
         api.del(artist1.href)
            .set('Authorization', userUtil.fakeAuthToken())
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should fail if user not owner', function (done) {
         api.del(artist1.href)
            .set('Authorization', userUtil.authToken(user5))
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should successfully DEL artist with alias', function (done) {
         api.del(artist1.href)
            .set('Authorization', userUtil.authToken(user1))
            .expect(function (res) {
               expect(res.body).to.deep.equal(artist1);
            })
            .expect(200)
            .end(function () {
               // verify you can't specifically GET the DELETEd doc
               api.get(artist1.href)
                  .expect(404)
                  .end(function() {
                     // verify that only one doc was DELETEd
                     api.get('/api/artist')
                        .expect(function (res) {
                           expect(res.body).to.be.an('array');
                           expect(res.body.length).to.be.gt(0);
                        })
                        .expect(200, done);
                  });
            });
      });

      it('should successfully DEL artist with ID and no alias', function (done) {
         api.del(artist2.href)
            .set('Authorization', userUtil.authToken(user1))
            .expect(function (res) {
               expect(res.body.name).to.deep.equal(artist2);
            })
            .expect(200)
            .end(function () {
               api.get(artist2.href)
                  .expect(404, done);
            });
      });

   });

   describe('Event Subdocument', function() {
      it('should do some artist event thing', function (done) {
      
         // We need to add Event subdocs for artists. these will essentially be event invitations
         done();
      
      });

      it('Another artist event test', function (done) {
      
         // Yet another one
         done();
      
      });
   });

   describe('Release Subdocument', function() {
      it('should do some artist release thing', function (done) {
      
         // We need to add Event subdocs for artists. these will essentially be event invitations
         done();
      
      });

      it('Another artist release test', function (done) {
      
         // Yet another one
         done();
      
      });
   });

   describe('Member Subdocument', function() {
      it('should add entity to the member list', function (done) {
         api.post(artist1.href + '/member/')
            .set('Authorization', userUtil.fakeAuthToken())
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should remove entity from the member list', function (done) {
         api.put(artist1.href)
            .set('Authorization', userUtil.authToken(user1))
            .send(artist1)
            .expect(function (res) {
               // Test and save updated date
               expect(artist1.updated).to.be.lt(res.body.updated);
               artist1.updated = res.body.updated;

               // Test other data
               expect(res.body).to.deep.equal(artist1);
            })
            .expect(200, done);
      });
   });

   describe('Owner Subdocument', function() {
      it('should successfully change the owner', function (done) {
         api.put(artist1.href + '/' + user2.id)
            .set('Authorization', userUtil.authToken(user1))
            .expect(function (res) {
               expect(res.body.artist.owner).to.deep.equal(user2);
            })
            .expect(200, done);
      });

      it('should fail to change owner', function (done) {
         artist1.name = 'changed';
         api.put(artist2.href + '/' + user2.id)
            .set('Authorization', userUtil.fakeAuthToken())
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });
   });

   describe('Admin Subdocument', function() {
      it('should fail to add an admin', function (done) {
         api.post(artist1.href)
            .set('Authorization', userUtil.fakeAuthToken())
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should add an admin', function (done) {
         api.post(artist1.href + '/' + user2.id)
            .set('Authorization', userUtil.authToken(user2))
            .expect(function (res) {
               expect(res.body.admins[0]).to.deep.equal(user2);
            })
            .expect(200, done);
      });


      it('should fail to remove an admin', function (done) {
         api.del(artist1.href + '/' + user2.id)
            .set('Authorization', userUtil.fakeAuthToken())
            .expect(function (res) {
               expect(res.body.errors).to.exist;
            })
            .expect(401, done);
      });

      it('should remove an admin', function (done) {
         api.del(artist1.href + '/' + user2.id)
            .set('Authorization', userUtil.authToken(user2))
            .expect(function (res) {
               expect(res.body.admins.length).to.equal(0);
            })
            .expect(200, done);
      });
  });
});

