var assert = require('chai').assert,
   config = require('config');

var ac = require('../../controllers/artist.js'),
   rc = require('../../controllers/release.js'),
   uc = require('../../controllers/user.js'),
   log = require('../../logging.js').logger;

describe('Event Unit Tests', function() {
   var j_user_1 = {
      email: 'unit_release@t.test',
      password: 'it w0Rks!'
   };

   var j_artist_1 = {
      name: 'Relase Test Artist 1',
      alias: 'releasetest1'
   };

   var j_release_1 = {
      name: 'Party day',
      created: (new Date()).toISOString()
   };

   var j_release_2 = {
      name: 'Songs about farts',
      created: (new Date()).toISOString()
   };

   var j_song_1 = {
      name: 'Everybody sharts',
      length: 10
   };

   var s_user_1;
   var artist_1;
   var release_1;
   var release_2;

   config.allowed_emails.push(j_user_1.email);

   before(function(done) {
      uc.postNew(j_user_1, function(err, user) {
         assert.isNotOk(err, err && err.errors);
         assert.equal(user.email, j_user_1.email);
         assert.notEqual(user.password, j_user_1.password);

         s_user_1 = user;
         done(); 
      });
   });

   before(function(done) {
      ac.postNew(j_artist_1, function(err, artist) {
         assert.isNotOk(err, err && err.errors);
         assert.equal(artist.name, j_artist_1.name);

         artist_1 = artist;
         done();
      });
   });

   describe('Create releases', function() {
      it('make a basic release', function(done) {
         j_release_1.owner = s_user_1;
         j_release_1.artist = artist_1.id;

         rc.postNew(j_release_1, function(err, release) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(release.name, j_release_1.name);

            release_1 = release;
            done();
         });
      });

      it('make a release with a song', function(done) {
         j_release_2.songs = [j_song_1];
         j_release_2.artist = artist_1.id;
         j_release_2.owner = s_user_1;

         rc.postNew(j_release_2, function(err, release) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(release.name, j_release_2.name);
            assert.equal(release.songs[0].name, j_song_1.name);

            release_2 = release;
            done();
         });
      });
   });

   describe('Get releases', function() {
      it('gets basic release by id', function(done){
         rc.getById(release_1.id, function(err, r) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(r.name, release_1.name);

            done();
         });
      });

      it('gets release with song by id', function(done){
         rc.getById(release_2.id, function(err, r) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(r.name, release_2.name);
            assert.equal(r.songs[0].name, j_song_1.name);

            done();
         });
      });

      it('get all releases', function(done) {
         rc.getAll({'artist': artist_1.id}, {}, 0, function(err, releases) {
            assert.equal(releases.length, 2);

            done();
         });
      });
   });

   describe('delete releases', function() {
      it('delete a release', function(done) {
         rc.deleteDocument(release_1, function(err, e){
            assert.isNotOk(err, err && err.errors);
            assert.equal(e.name, release_1.name);

            done();
         });
      });
   });
});
