var assert = require('chai').assert,
   config = require('config');

var ac = require('../../controllers/artist.js'),
   uc = require('../../controllers/user.js'),
   log = require('../../logging.js').logger;

describe('Artist Unit Tests', function() {
   var j_user_1 = {
      email: 'unit_artist@t.test',
      password: 'it w0Rks!'
   };

   var j_artist_1 = {
      name: 'Test Artist 1',
      alias: 'test1'
   };

   var a_user_1;
   var artist_1;

   config.allowed_emails.push(j_user_1.email);

   before(function(done) {
      uc.postNew(j_user_1, function(err, user) {
         assert.isNotOk(err, err && err.errors);
         assert.equal(user.email, j_user_1.email);
         assert.notEqual(user.password, j_user_1.password);

         a_user_1 = user;
         j_artist_1.owner = user.id;
         done(); 
      });
   });

   describe('Create artists', function() {
      it('make a basic artist', function(done) {
         ac.postNew(j_artist_1, function(err, artist) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(artist.name, j_artist_1.name);

            artist_1 = artist;
            done();
         });
      });
   });

   describe('Get artists', function() {
      it('gets artist by id', function(done){
         ac.getById(artist_1.id, function(err, a) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(a.name, artist_1.name);

            done();
         });
      });

      it('gets artist by alias', function(done){
         ac.getBy('alias', artist_1.alias, function(err, a) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(a.name, artist_1.name);

            done();
         });
      });

      it('parse artist doc', function(done){
         ac.parseDocument(j_artist_1, function(err, a) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(a.name, artist_1.name);

            done();
         });
      });
   });

   describe('delete artists', function() {
      it('delete an artist', function(done) {
         ac.deleteDocument(artist_1, function(err, a){
            assert.isNotOk(err, err && err.errors);
            assert.equal(a.name, artist_1.name);

            done();
         });
      });
   });
});
