var assert = require('chai').assert,
   config = require('config');

var ec = require('../../controllers/event.js'),
   uc = require('../../controllers/user.js'),
   log = require('../../logging.js').logger;

describe('Event Unit Tests', function() {
   var j_user_1 = {
      email: 'unit_event@t.test',
      password: 'it w0Rks!'
   };

   var j_event_1 = {
      name: 'Party day',
      start_date: (new Date()).toISOString()
   };

   var a_user_1;
   var event_1;

   config.allowed_emails.push(j_user_1.email);

   before(function(done) {
      uc.postNew(j_user_1, function(err, user) {
         assert.isNotOk(err, err && err.errors);
         assert.equal(user.email, j_user_1.email);
         assert.notEqual(user.password, j_user_1.password);

         a_user_1 = user;
         j_event_1.owner = user.id;
         done(); 
      });
   });

   describe('Create event', function() {
      it('make a basic event', function(done) {
         ec.postNew(j_event_1, function(err, event) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(event.name, j_event_1.name);

            event_1 = event;
            done();
         });
      });
   });

   describe('Get events', function() {
      it('gets event by id', function(done){
         ec.getById(event_1.id, function(err, e) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(e.name, event_1.name);

            done();
         });
      });
   });

   describe('delete events', function() {
      it('delete an event', function(done) {
         ec.deleteDocument(event_1, function(err, e){
            assert.isNotOk(err, err && err.errors);
            assert.equal(e.name, event_1.name);

            done();
         });
      });
   });
});
