var assert = require('chai').assert,
   config = require('config');

var pc = require('../../controllers/post.js'),
   log = require('../../logging.js').logger;

describe('Post Unit Tests', function() {
   var j_user_1 = {
      email: 'unit_post@t.test',
      password: 'it w0Rks!'
   };

   var j_post_1 = {
      title: 'Test Post 1',
   };

   var a_user_1;
   var post_1;

   config.allowed_emails.push(j_user_1.email);

   describe('Create posts', function() {
      it('make a basic post', function(done) {
         pc.postNew(j_post_1, function(err, post) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(post.title, j_post_1.title);

            post_1 = post;
            done();
         });
      });
   });

   describe('Get posts', function() {
      it('gets post by id', function(done){
         pc.getById(post_1.id, function(err, p) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(p.title, post_1.title);

            done();
         });
      });
   });

   describe('delete posts', function() {
      it('delete an post', function(done) {
         pc.deleteDocument(post_1, function(err, p){
            assert.isNotOk(err, err && err.errors);
            assert.equal(p.title, post_1.title);

            done();
         });
      });
   });
});
