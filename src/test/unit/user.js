var assert = require('chai').assert
   config = require('config');

var uc = require('../../controllers/user.js'),
   log = require('../../logging.js').logger;

describe('User Unit Tests', function() {
   var j_user_1 = {
      email: 'unit_test_create@t.test',
      password: 'it w0Rks!'
   };
   
   config.allowed_emails.push(j_user_1.email)

   describe('create users', function(){
      it('make a basic user', function(done) {
         uc.postNew(j_user_1, function(err, user) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(user.email, j_user_1.email);
            assert.notEqual(user.password, j_user_1.password);

            done();
         })
      });
   });

   describe('get users', function() {
      it('get a user', function(done) {
         uc.getBy('email', j_user_1.email, function(err, user) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(user.email, j_user_1.email);
            assert.notEqual(user.password, j_user_1.password);

            done();
         })
      });

      it('parse user doc', function(done) {
         uc.parseDocument(j_user_1, function(err, user) {
            assert.isNotOk(err, err && err.errors);
            assert.equal(user.email, j_user_1.email);
            assert.notEqual(user.password, j_user_1.password);

            done();
         })
      });
   });

   describe('delete users', function() {
      it('delete a user', function(done) {
         uc.parseDocument(j_user_1, function(err, user) {
            uc.deleteDocument(user, function(err, del_user){
               uc.getBy('email', j_user_1.email, function(err, user) {
                  assert.isOk(err);
                  assert.isNotOk(user);

                  done();
               });
            });
         });
      });
   });
});
