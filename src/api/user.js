var uc = require('../controllers/user.js'),
   baseHandler = require('./handler.js'),
   followMap = require('../mongo/follow_map.js'),
   emailer = require('../emailer'),
   sendError = require('../util/error.js').sendError;

var userHandler = baseHandler(uc);

userHandler.AuthMiddleware = function AuthMiddleware(req, res, next) {
   if (!req.user || req.user.id !== req.params.id) {
      // No error handlers in express, so send response here
      //return next(new AuthError('Unauthorized access.'));
      return res.status(401).json({
         errors: ['Unauthorized Access']
      });
   }

   req.populated.user = req.user;

   next();
};

userHandler.getSubscribed = function getSubscribed(req, res) {
   var page = req.query.page || 0;

   followMap.find({follower: req.user.id}).skip(page * 25).limit(25)
   .then(subscriptions => {
      res.json({subscriptions});
   }).catch(error => {
      res.status(500).json({error});
   });
};

userHandler.populateByEmail = function populateByEmail(req, res, next) {
   uc.getByIdOr('email', req.params.id, function (err, mongoDoc) {
      if (err) return sendError(err, res);

      req.populated.user = mongoDoc;
      next();
   });
};

userHandler.loggedInOnly = function loggedInOnly(req, res, next) {
   if (req.user) return next();
   res.status(403).json({
      errors: 'You must be logged in to access this route.'
   });
};

userHandler.filterUpdate = function filterUpdate(req, res, next) {
   // Delete nested objects
   delete(req.body.owned_artists);
   delete(req.body.admin_artists);
   delete(req.body.owned_events);
   delete(req.body.admin_events);

   next();
};

userHandler.initPasswordReset = function initPasswordReset(req, res, next) {
   var user = req.populated.user;

   // Always send this. We don't want to give away that a user (doesn't) exist with this email
   // No need to send any content (they'll get an email), so we 204
   res.status(204).send();

   if (!user) return;

   var token = uc.initPasswordReset(user);
   emailer.sendPasswordReset(token, user.email);
};

userHandler.resetPassword = function resetPassword(req, res, next) {
   var token = req.params.token,
      user = req.populated.user;

   uc.resetPassword(token, user, req.body.password, function (err, user) {
      if (err) return res.status(400).json({
         errors: err
      });

      res.json(user);
   });
};

module.exports = userHandler;
