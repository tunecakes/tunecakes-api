var rc = require('../controllers/request.js'),
   baseHandler = require('./handler.js');

var requestHandler = baseHandler(rc);

requestHandler.postNew = (req, res) => {
   req.body.user_origin = req.user;
   rc.postNew(req.body, function(err, data) {
      if(err) {
         return res.status(500).json(err);
      }

      res.json(data);
   });
};

module.exports = requestHandler;
