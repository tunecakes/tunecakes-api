var mongoose = require('mongoose'),
   UserCtrl = require('../controllers/user.js'),
   User = mongoose.model('user'),
   jwt = require('jsonwebtoken'),
   config = require('config'),
   log = require('../logging.js').logger,
   constants = require('../constants'),
   AuthError = require('../error').AuthError,
   expressJwt = require('express-jwt');

module.exports = {
   authenticate: function (req, res, next) {
      function sendError(code, err) {
         res.status(code).json({errors: err});
      }

      log.info(req.body.email);

      UserCtrl.getBy('email', req.body.email, function (err, user) {
         if (err) {
            log.warn('Mongo error on Login', err);
            return sendError(500, err);
         }
         if (!user) {
            log.warn('Login failed. User does not exist with email ' + req.body.email);
            return sendError(401, ['Invalid username or password']);
         }
         user.comparePassword(req.body.password, function (error, isMatch) {
            if (error) {
               log.warn('Error occurred comparing passwords.', error);
               return sendError(401, [error.message]);
            }
            if (isMatch) {
               log.info(user, 'Login success');
               return res.json({
                  token: jwt.sign({
                     user_id: user._id
                  }, config.get("jwt_key"), {
                     expiresIn: "7 days"
                  }),
                  userId: user._id
               });
            } else {
               log.info(user, 'Login failure');
               return sendError(401, ['Invalid username or password']);
            }
         });
      });
   },

   checkAuth: function (req, res, next) {
      expressJwt({
         secret: config.get('jwt_key')
      })(req, res, function (err) {
         if (err) {
            log.warn('Auth token failed parsing.', err.message);
            return next();
         }

         log.debug('USER', req.user, 'Auth token successfully parsed');

         UserCtrl.getById(req.user.user_id, function (err, user) {
            if (err) log.error(err);
            req.user = user;
            next();
         });
      });
   }
};
