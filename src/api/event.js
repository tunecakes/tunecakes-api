var ec = require('../controllers/event.js'),
   eventFeed = require('../feed/event.js'),
   constants = require('../constants'),
   feed_constants = constants.feeds,
   getstream = require('../util/getstream.js'),
   ArtistController = require('../controllers/artist.js'),
   baseHandler = require('./handler.js'),
   uc = require('../controllers/user.js');

var eventHandler = baseHandler(ec);

eventHandler.postNew = function postNewEvent(req, res) {
   req.body.owner = req.user.id;
   ec.postNew(req.body, function (err, event) {
      event.linked_artists.forEach(function (artist) {
         ArtistController.eventCreatedActivity(artist, event);
      });

      res.json(ec.filterDocument(event, req.user));
   });
};

eventHandler.editOwner = function editOwner(req, res, next) {
   var event = req.populated.event;
   var owner = req.populated.user;
   var old_owner = req.user;

   event.owner = owner.id;
   event.save();

   owner.owned_events.addToSet(event.id);
   owner.save();

   old_owner.owned_events.pull(event.id);
   old_owner.save();

   res.json(event.id);
};

eventHandler.addAdmin = function addAdmin(req, res, next) {
   var event = req.populated.event;
   var admin = req.populated.user;

   event.admins.addToSet(admin.id);
   event.save();

   admin.admin_events.addToSet(event.id);
   admin.save();

   res.json(event);
};

eventHandler.removeAdmin = function removeAdmin(req, res, next) {
   var event = req.populated.event;
   var admin = req.populated.user;

   event.admins.pull(admin.id);
   event.save();

   admin.admin_events.pull(event.id);
   admin.save();

   res.json(event);
};

eventHandler.addArtist = function addArtist(req, res, next) {
   var event = req.populated.event;
   var artist = req.populated.artist;

   ArtistController.addEvent(artist, event);
   ArtistController.eventCreatedActivity(artist, event);

   res.json(event);
};

eventHandler.removeArtist = function removeArtist(req, res, next) {
   var event = req.populated.event;
   var artist = req.populated.artist;

   event.artists.pull(artist);
   event.save();
   artist.events.pull(event);
   artist.save();

   res.json(event);
};

eventHandler.postFollower = function eventPostFollower(req, res) {
   var followMap = eventFeed.addFollower(req.populated.event, req.user, req.body.flags);
   res.json(followMap);
};

eventHandler.updateFollower = function eventUpdateFollower(req, res) {
   var flags = req.body.flags,
      removedFlags, addedFlags;
   var event = req.populated.event;

   var user = req.user;
   var userNotiFeed = uc.getNotificationFeed(user);

   ec.getFollower(event, user, function (err, followMap) {
      if (err) return res.status(500).json(err);

      removedFlags = _.difference(followMap.flags, flags);
      addedFlags = _.difference(flags, followMap.flags);

      removedFlags.forEach(function (flag) {
         var feed = getstream.getFeed(feed_constants.event[flag], event.id);
         getstream.unfollow(userNotiFeed, feed);
      });

      addedFlags.forEach(function (flag) {
         var feed = getstream.getFeed(feed_constants.event[flag], event.id);
         getstream.follow(userNotiFeed, feed);
      });

      followMap.flags = flags;
      followMap.save();
      res.json(followMap);
   });
};

eventHandler.deleteFollower = function eventDeleteFollower(req, res) {
   var event = req.populated.event;
   var eventFlatFeeds = ec.getFlatFeeds(event);

   var user = req.user;
   var userNotiFeed = uc.getNotificationFeed(user);
   var userAggFeed = uc.getAggregateFeed(user);

   ec.getFollower(event, user, function (err, followMap) {
      if (err) return res.status(500).json(err);

      eventFlatFeeds.forEach(function (feed) {
         getstream.unfollow(userNotiFeed, feed);
         getstream.unfollow(userAggFeed, feed);
      });

      followMap.remove();
      res.json(followMap);
   });
};

module.exports = eventHandler;
