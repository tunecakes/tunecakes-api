var elasticWrapper = require('../util/elastic_wrapper'),
   log = require('../logging.js').logger;

function artistSearch(req, res) {
   var {text, tags, latitude, longitude, radius, page, random} = req.query;
   var now = new Date();

   if(random) {
      random = random.toLowerCase() === 'true'? true : false;
   }

   now.setHours(0);
   now.setMinutes(0);
   now.setSeconds(0);

   var query = {
      query: {
         bool: { filter: [],
            must: [],
            should: []
         }
      }
   };

   if (!text || text.trim() === '') {
      query.query.bool.must.push({match_all: {}});
   } else {
      query.query.bool.should.push({
         match: {
            'name': {
               query: text,
               fuzziness: 'AUTO',
               prefix_length: 0,
               max_expansions: 100
            }
         }
      });
   }

   if (page) {
      query.from = page * 10;
   }
   
   if (latitude && longitude && radius) {
      query.query.bool.filter.push({
         geo_distance: {
            distance: radius,
            location: {
               lat: latitude,
               lon: longitude
            }
         }
      });
   } else if (latitude && longitude) {
      query.query.bool.must.push({
         function_score: {
            query: {
               exists: {
                  field: "location"
               }
            },
            boost: 1
         }
      });

      query.query.bool.must.push({
         function_score: {
            functions: [{
               gauss: {
                  location: {
                     origin: {
                        lat: latitude,
                        lon: longitude
                     },
                     offset: '10km',
                     scale: '500km'
                  }
               }
            }]
         }
      });
   }

   if (tags) {
      if (typeof tags === 'string') tags = [tags];

      query.query.bool.filter.push({terms: {tags: tags}});
      query.query.bool.should.push({
         terms: {
            tags: tags,
            boost: 0.2
         }
      });
   }

   if (random) {
      query.query.bool.must.push({
         function_score: {
            query : { match_all: {} },
            random_score : {},
            boost: 0.5,
            boost_mode: "sum"
         }
      });
   }

   elasticWrapper.search('search', 'artist', query, function (err, data) {
      if (err) {
         log.error('Elastic search failed.', err);

         return res.status(err.status).json({
            error: err.message || 'Unknown Elasticsearch error'
         });
      }
      res.json(data);
   });
}

function eventSearch(req, res) {
   var {text, tags, latitude, longitude, radius, page} = req.query;
   var now = new Date();

   now.setHours(0);
   now.setMinutes(0);
   now.setSeconds(0);

   var query = {
      query: {
         bool: {
            filter: [{
               range: {
                  start_date: {
                     gte: now
                  }
               }
            }],
            must: [],
            should: []
         }
      }
   };

   if (!text || text.trim() === '') {
      query.query.bool.must.push({match_all: {}});
   } else {
      query.query.bool.must.push({
         nested: {
            path: 'linked_artists',
            query: {
               bool: {
                  must: [{
                     match: {
                        'linked_artists.name': {
                           query: text,
                           fuzziness: 'AUTO',
                           prefix_length: 0,
                           max_expansions: 100
                        }
                     }
                  }]
               }
            }
         }
      });
   }

   if (page) {
      query.from = page * 10;
   }
   
   if (latitude && longitude && radius) {
      query.query.bool.filter.push({
         geo_distance: {
            distance: radius,
            location: {
               lat: latitude,
               lon: longitude
            }
         }
      });
   } else if (latitude && longitude) {
      query.query.bool.must.push({
         function_score: {
            functions: [{
               gauss: {
                  location: {
                     origin: {
                        lat: latitude,
                        lon: longitude
                     },
                     offset: '10km',
                     scale: '500km'
                  }
               }
            }]
         }
      });
   }
   
   if (tags) {
      if (typeof tags === 'string') tags = [tags];

      query.query.bool.filter.push({terms: {tags: tags}});
      query.query.bool.should.push({
         terms: {
            tags: tags,
            boost: 0.2
         }
      });
   }

   elasticWrapper.search('search', 'event', query, function (err, data) {
      if (err) {
         log.error('Elastic search failed.', err);

         return res.status(err.status).json({
            error: err.message || 'Unknown Elasticsearch error'
         });
      }
      res.json(data);
   });
}

module.exports = {
   artistSearch,
   eventSearch
};
