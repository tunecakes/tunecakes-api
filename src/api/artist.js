var ac = require('../controllers/artist.js'),
   config = require('config'),
   follow_map = require('../mongo/follow_map.js'),
   artistFeed = require('../feed/artist.js'),
   uc = require('../controllers/user.js'),
   constants = require('../constants'),
   getstream = require('../util/getstream.js'),
   baseHandler = require('./handler.js'),
   feed_constants = constants.feeds,
   facebook = require('../facebook'),
   log = require('../logging.js').logger,
   _ = require('underscore'),
   twitter = require('../twitter');

var artistHandler = baseHandler(ac);

artistHandler.injectFollowerCount = (req, res, next) => {
   var artist = req.populated.artist;

   follow_map.count({publisher: artist.id}, (err, count) => {
      artist.followers = count;
      next();
   });
};

artistHandler.editOwner = function editOwner(req, res, next) {
   var artist = req.populated.artist;
   var owner = req.populated.user;
   var old_owner = req.user;

   artist.owner = owner.id;
   artist.save();

   owner.owned_artists.addToSet(artist.id);
   owner.save();

   old_owner.owned_artists.pull(artist.id);
   old_owner.save();

   res.json(artist);
};

artistHandler.addAdmin = function addAdmin(req, res, next) {
   var artist = req.populated.artist;
   var admin = req.populated.user;

   artist.admins.addToSet(admin.id);
   artist.save();

   admin.admin_artists.addToSet(artist.id);
   admin.save();

   res.json(artist);
};

artistHandler.removeAdmin = function removeAdmin(req, res, next) {
   var artist = req.populated.artist;
   var admin = req.populated.user;

   artist.admins.pull(admin.id);
   artist.save();

   admin.admin_artists.pull(artist.id);
   admin.save();

   res.json(artist);
};

artistHandler.postFollower = function artistPostFollower(req, res) {
   artistFeed.addFollower(req.populated.artist, req.user, req.body.flags)
   .then(followMap => {
      res.json(followMap);
   }).catch(error => {
      res.status(500).json({error});
   });
};

artistHandler.updateFollower = function artistUpdateFollower(req, res) {
   var flags = req.body.flags, removedFlags, addedFlags;
   var artist = req.populated.artist;

   var user = req.user;
   var userNotiFeed = uc.getNotificationFeed(user);

   ac.getFollower(artist, user, function (err, followMap) {
      if(err) return res.status(500).json(err);

      removedFlags = _.difference(followMap.flags, flags),
      addedFlags = _.difference(flags, followMap.flags);

      removedFlags.forEach(function(flag) {
         var feed = getstream.getFeed(feed_constants.artist[flag], artist.id);
         getstream.unfollow(userNotiFeed, feed);
      });

      addedFlags.forEach(function(flag) {
         var feed = getstream.getFeed(feed_constants.artist[flag], artist.id);
         getstream.follow(userNotiFeed, feed);
      });

      followMap.flags = flags;
      followMap.save();
      res.json(followMap);
   });
};

artistHandler.deleteFollower = function artistDeleteFollower(req, res) {
   var artist = req.populated.artist;
   var artistFlatFeeds = ac.getFlatFeeds(artist);

   var user = req.user;
   var userNotiFeed = uc.getNotificationFeed(user);
   var userAggFeed = uc.getAggregateFeed(user);

   ac.getFollower(artist, user, function (err, followMap) {
      if(err) return res.status(500).json(err);

      artistFlatFeeds.forEach(function(feed) {
         getstream.unfollow(userNotiFeed, feed);
         getstream.unfollow(userAggFeed, feed);
      });

      followMap.remove();
      res.json(followMap);
   });
};

artistHandler.facebookLogin = function facebookLogin(req, res, next) {
   var artist = req.populated.artist,
      accessToken = req.body.access_token,
      pageId = req.body.facebook_id;

   facebook.getExtendedToken(accessToken)
   .then(function(token) {
      return facebook.getPageToken(pageId, token);
   }).then(function(token) {
      artist.facebook_id = pageId;
      artist.facebook_url = 'https://www.facebook.com/' + pageId;
      artist.social_accounts.facebook = {
         id: pageId,
         access_token: token
      };

      artist.save();

      res.json(artist);
   }).catch(function(err) {
      log.error('FACEBOOK ERROR', err);
      res.status(503).json(err);
   });
};

artistHandler.twitterAuth = function twitterAuth(req, res, next) {
   var artist = req.populated.artist,
      callbackUrl = config.twitter.callback + '/artist/' + (artist.alias || artist.id) + '/twitter/callback';

   twitter.getOAuthRequestToken(callbackUrl)
   .then(function(oauth) {
      var twitterAuthUrl = twitter.getAuthorizationUrl(oauth.oauth_token);
      res.json({'twitter_url': twitterAuthUrl});
   }).catch(function(err) {
      res.status(503).json(err);
   });
};

artistHandler.twitterCallback = function twitterCallback(req, res, next) {
   var artist = req.populated.artist;

   twitter.parseVerifierToken(req.body.oauth_verifier, req.body.oauth_token)
   .then(function(oauth){
      log.info('Twitter OAuth Response', oauth);

      artist.social_accounts.twitter = oauth;
      artist.twitter_id = oauth.user_id;
      artist.twitter_url = 'https://twitter.com/' + oauth.screen_name;
      artist.save();
      res.json(artist);
   }).catch(function(err) {
      res.status(503).json(err);
   });
};

artistHandler.addBlogToFeed = function addBlogToFeed(req, res, next) {
   var artist = req.populated.artist,
      post = req.populated.post;
   
   ac.blogPostCreatedActivity(artist, post),
   next();
};

module.exports = artistHandler;
