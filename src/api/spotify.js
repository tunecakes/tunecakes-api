var config = require('config'),
   Promise = require('bluebird'),
   querystring = require('querystring'),
   request = require('request-promise');

const scope = 'user-follow-read';

var redirectToAuth = (req, res) => {
   var url = 'https://accounts.spotify.com/authorize?';
   url += querystring.stringify({
      response_type: 'code',
      client_id: config.spotify.clientID,
      scope: scope,
      redirect_uri: config.spotify.callbackURL
   });
   res.json({url});
};

var checkForRefresh = (req, res, next) => {
   var {user} = req.populated,
      now = new Date();

   if(user.spotify.expires_at > now) {
      return next();
   }

   refreshToken(user)
   .then(next)
   .catch(err => {
      res.status(503).json(err);
   });
};

var getFollowedArtists = (req, res) => {
   var {user} = req.populated;
   
   getFollowedArtistsFromUser(user)
   .then(artists => {
      res.json(artists);
   })
   .catch(error => {
      res.status(503).json({error});
   });
};

var saveAuthToken = (req, res) => {
   var {code} = req.body;
   var {user} = req.populated;
   var expiresAt = new Date();

   getAccessToken(code)
   .then(tokens => {
      // Store when this token expires so we know when to refresh
      expiresAt.setSeconds(expiresAt.getSeconds() + tokens.expires_in);
      tokens.expires_at = expiresAt;

      // Add tokens to user data and save
      user.spotify = tokens;
      user.save();

      res.json(user);
   })
   .catch(error => {
      res.status(503).json({error});
   });
};

var refreshToken = (user) => {
   var expiresAt = new Date();
   var apiAuth = config.spotify.clientID + ':' + config.spotify.clientSecret;
   var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
         refresh_token: user.spotify.refresh_token,
         grant_type: 'refresh_token'
      },
      headers: {
         'Authorization': 'Basic ' + new Buffer(apiAuth).toString('base64')
      },
      json: true
   };

   // Get new tokens
   return request.post(authOptions)
   .then(tokens => {
      // Store when this token expires so we know when to refresh
      expiresAt.setSeconds(expiresAt.getSeconds() + tokens.expires_in);
      tokens.expires_at = expiresAt;
      tokens.refresh_token = tokens.refresh_token || user.spotify.refresh_token;

      // Add tokens to user data and save
      user.spotify = tokens;
      user.save();
   });
};

var getAccessToken = (code) => {
   var apiAuth = config.spotify.clientID + ':' + config.spotify.clientSecret;

   var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
         code: code,
         redirect_uri: config.spotify.callbackURL,
         grant_type: 'authorization_code'
      },
      headers: {
         'Authorization': 'Basic ' + new Buffer(apiAuth).toString('base64')
      },
      json: true
   };

   // Get new tokens
   return request.post(authOptions);
};

var getFollowedArtistsFromUser = (user) => {
   var options = {
      url: 'https://api.spotify.com/v1/me/following',
      qs: {
         type: 'artist',
         limit: 50
      },
      headers: {
         'Authorization': 'Bearer ' + user.spotify.access_token
      },
      json: true
   };

   return request(options)
   .then(res => {
      return handleGetFollowedArtistsReponse(res, user);
   });
};

var handleGetFollowedArtistsReponse = (response, user) => {
   if(!response.artists) {
      return Promise.resolve([]);
   } else if(!response.artists.next) {
      return Promise.resolve(response.artists.items);
   }

   var options = {
      url: response.artists.next,
      headers: {
         'Authorization': 'Bearer ' + user.spotify.access_token
      },
      json: true
   };

   return request(options)
   .then(res => {
      return handleGetFollowedArtistsReponse(res, user).then(new_artists => {
         return Promise.resolve(response.artists.items.concat(new_artists));
      });
   });
};

module.exports = {
   redirectToAuth,
   saveAuthToken,
   checkForRefresh,
   getFollowedArtists
};
