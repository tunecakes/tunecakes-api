var pc = require('../controllers/post.js'),
   baseHandler = require('./handler.js'),
   sendError = require('../util/error.js').sendError,
   log = require('../logging.js').logger,
   facebook = require('../facebook'),
   twitter = require('../twitter');

var postHandler = baseHandler(pc);

postHandler.getAll = function(entityType) {
   return function(req, res, next) {
      var entity = req.populated[entityType];
      var {sort, page} = req.query;

      sort = sort || {};
      if (typeof page !== 'number') {
         page = 0;
      }

      pc.getAll({ entity_id: entity.id }, sort, page, function(err, posts) {
         if(err) return sendError(err, res);
         
         res.json(posts);
      });
   };
};


postHandler.populateById = function(entityType) {
   return function(req, res, next) {
      pc.getById(req.params.id, function(err, mongoDoc) {
         if(err) return sendError(err, res);

         req.populated[pc.model.name] = mongoDoc;
         next();
      });
   };
};

postHandler.postNew = function(entityType) {
   return function(req, res, next) {
      var entity = req.populated[entityType];

      req.body.author = req.user;
      req.body.entity_id = entity.id;
      req.body.entity_type = entity._type;

      pc.postNew(req.body, function(err, post) {
         if(err) return sendError(err, res);

         entity.posts.addToSet(post);
         entity.save();

         res.json(post);
         req.populated.post = post;
         next();
      });
   };
};

postHandler.socialMediaFlags = function(req, res, next) {
   if(req.body.twitter) {
      req.useTwitter = true;
      delete(req.body.twitter);
   }

   if(req.body.facebook) {
      req.useFacebook = true;
      delete(req.body.facebook);
   }

   next();
};

postHandler.twitter = function(entityType) {
   return function(req, res, next) {
      var entity = req.populated[entityType],
         post = req.populated.post;

      if(!req.useTwitter) return next();
      if(!entity.social_accounts.twitter) return next();

      twitter.tweet(post.tweet_text, entity.social_accounts.twitter.oauth_token, entity.social_accounts.twitter.oauth_token_secret)
      .then(function(tweet) {
         log.debug('Successfully tweeted', tweet);

         post.twitter_id = tweet.id_str;
         post.save();
      })
      .catch(function(err) {
         log.error('Error sending tweet', err.message);
      });

      next();
   };
}

postHandler.facebook = function(entityType) {
   return function(req, res, next) {
      var entity = req.populated[entityType],
         post = req.populated.post;

      if(!req.useFacebook) return next();
      if(!entity.social_accounts.facebook) return next();

      facebook.postToPage(post.text, entity.social_accounts.facebook.access_token)
      .then(function(fbPost) {
         log.debug('Successfully posted to Facebooked', fbPost);

         post.facebook_id = fbPost.id;
         post.save();
      })
      .catch(function(err) {
         log.error('Error posting to facebook', err);
      });

      next();
   };
}

postHandler.update = function(entityType) {
   return function(req, res, next) {
      var entity = req.populated[entityType];

      if(req.populated.post.entity_type !== entityType) {
         return sendError('Failed to find post', res);
      }

      req.body.author = req.user;
      req.body.entity_id = entity.id;
      req.body.entity_type = entity._type;

      pc.updateDocument(req.body, req.populated[pc.model.name], function(err, data) {
         if(err) return sendError(err, res);

         res.json(data);
      });
   }
};

postHandler.delete = function(entityType) {
   return function(req, res, next) {
      var entity = req.populated[entityType];
      var post = req.populated.post;

      if(post.entity_type !== entityType) {
         return sendError('Failed to find post', res);
      }
      
      entity.posts.pull(post);
      entity.save();

      post.remove();
      res.json(post);
   }
};

module.exports = postHandler;
