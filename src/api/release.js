var rc = require('../controllers/release.js'),
   sc = require('../controllers/song.js'),
   baseHandler = require('./handler.js');

var releaseHandler = baseHandler(rc);

releaseHandler.editOwner = function editOwner(req, res, next) {
   var release = req.populated.release;
   var owner = req.populated.user;
   var old_owner = req.user;

   release.owner = owner.id;
   release.save();

   owner.owned_releases.addToSet(release.id);
   owner.save();

   old_owner.owned_releases.pull(release.id);
   old_owner.save();

   res.json(release.id);
};

releaseHandler.addAdmin = function addAdmin(req, res, next) {
   var release = req.populated.release;
   var admin = req.populated.user;

   release.admins.addToSet(admin.id);
   release.save();

   admin.admin_releases.addToSet(release.id);
   admin.save();

   res.json(release);
};

releaseHandler.removeAdmin = function removeAdmin(req, res, next) {
   var release = req.populated.release;
   var admin = req.populated.user;

   release.admins.pull(admin.id);
   release.save();

   admin.admin_releases.pull(release.id);
   admin.save();

   res.json(release);
};

releaseHandler.addArtist = function addArtist(req, res, next) {
   var release = req.populated.release;
   var artist = req.populated.artist;

   release.linked_artists.addToSet(artist.id);
   release.save();

   // What should we name the new model field?
   //artist.releases.addToSet(release.id).save();

   res.json(release);
};

releaseHandler.removeArtist = function removeArtist(req, res, next) {
   var release = req.populated.release;
   var artist = req.populated.artist;

   release.contributing_artists.pull(artist.id);
   release.save();

   // What should we name the new model field?
   //artist.releases.pull(release)..save();

   res.json(release);
};

releaseHandler.addSong = function addSong(req, res, next) {
   var release = req.populated.release;

   sc.postNew(req.body, function (err, song) {
      if (err) return res.status(400).json(err);

      release.songs.addToSet(song);

      console.log("SONG WHAT THE FUCK", song, release)

      release.save();

      res.json(song);
   });
};

releaseHandler.removeSong = function removeSong(req, res) {
   var release = req.populated.release,
      song = req.populated.song;

   release.songs.pull(song);
   release.save();

   song.remove();
   res.json(song);
};

releaseHandler.populateSong = function populateSong(req, res, next) {
   sc.getById(req.params.id, function (err, song) {
      if (err) return res.status(400).json(err);
      
      if (!song) return res.status(404).json({'error': 'Cannot find song by ID'});
      
      req.populated.song = song;
      next();
   });
};

module.exports = releaseHandler;
