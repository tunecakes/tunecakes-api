var express = require('express'),
   app = module.exports = express(),
   config = require('config'),
   log = require('../logging.js').logger,
   auth = require('./authentication');

var ArtistHandler = require('./artist.js'),
   EventHandler = require('./event.js'),
   ReleaseHandler = require('./release.js'),
   RequestHandler = require('./request.js'),
   PostHandler = require('./post.js'),
   SearchHandler = require('./search.js'),
   SpotifyHandler = require('./spotify.js'),
   UserHandler = require('./user.js');

var routeParams = {
   mergeParams: true
};
/**
 * @swagger
 * securityDefinitions:
 *   jwt_token:
 *     type: oauth
 *     name: jwt_token
 *     in: header
 */
app.post('/authenticate', auth.authenticate);

// This middleware will create the req.user object
app.use(auth.checkAuth);

// Prepare req.populated object
app.use(function (req, res, next) {
   req.populated = {};
   next();
});

app.get('/user/model/:role', UserHandler.fieldMetadata);
app.post('/user', UserHandler.validate, UserHandler.postNew);
app.post('/validate/user', UserHandler.validate, UserHandler.validateSuccess);
app.post('/validate/user/:id', UserHandler.populateByEmail, UserHandler.checkOwnerPermissions, UserHandler.validate, UserHandler.validateSuccess);

var userRouter = express.Router(routeParams);
app.use('/user/:id', userRouter);
userRouter.use(UserHandler.populateByEmail);
userRouter.post('/password_reset', UserHandler.initPasswordReset);
userRouter.post('/password_reset/:token', UserHandler.resetPassword);
/*
 * User routes after here require a logged in user
 */
userRouter.use('/', UserHandler.loggedInOnly, UserHandler.AuthMiddleware, UserHandler.checkOwnerPermissions);
userRouter.get('/', UserHandler.getById);
userRouter.put('/', UserHandler.filterUpdate, UserHandler.validate, UserHandler.update);
userRouter.delete('/', UserHandler.delete);
userRouter.get('/follow', UserHandler.getSubscribed);
userRouter.get('/feed/:type', UserHandler.getFeed, UserHandler.transformFeed);
userRouter.get('/spotify', SpotifyHandler.redirectToAuth);
userRouter.post('/spotify', SpotifyHandler.saveAuthToken);
userRouter.get('/spotify/followed_artists', SpotifyHandler.checkForRefresh, SpotifyHandler.getFollowedArtists);

/* 
 * All PUTs and DELETEs below here require a user to be logged in
 */
app.put('*', UserHandler.loggedInOnly);
app.delete('*', UserHandler.loggedInOnly);

/**
 *  @swagger
 *  /artist:
 *    post:
 *      tags:
 *        - artist
 *      summary: Create new artist
 *      description: ""
 *      consumes:
 *        - application/json
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Newly created artist
 *          schema:
 *            $ref: "#/definitions/Artist"
 *      parameters:
 *        - in: body
 *          name: body
 *          description: Artist object to be created
 *          required: true
 *
 *          schema:
 *            $ref: "#/definitions/Artist"
 *      security:
 *        - api_key: []
 *    get:
 *      tags:
 *        - artist
 *      summary: Query available artists
 *      description: ""
 *      produces:
 *        - application/json
 *      responses:
 *        "200":
 *          description: Array of available artists
 *          schema:
 *            type: array
 *            $ref: "#/definitions/Artist"
 */
app.get('/artist', ArtistHandler.getAll);
app.post('/artist', UserHandler.loggedInOnly, ArtistHandler.validate, ArtistHandler.postNew);
app.post('/validate/artist', UserHandler.loggedInOnly, ArtistHandler.validate, ArtistHandler.validateSuccess);
app.post('/validate/artist/:id', UserHandler.loggedInOnly, ArtistHandler.populateByAlias, ArtistHandler.checkAdminPermissions, ArtistHandler.validate, ArtistHandler.validateSuccess);
app.get('/artist/model/:role', ArtistHandler.fieldMetadata);

/**
 *  @swagger
 *  /artist/{artistIdOrAlias}:
 *    get:
 *      tags:
 *        - artist
 *      summary: Get artist by id or alias
 *      description: ""
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Artist matching ID or alias
 *          schema:
 *            $ref: "#/definitions/Artist"
 *        400:
 *          description: Error occurred
 *          schema:
 *            type: object
 *        404:
 *          description: Not found
 *          schema:
 *            type: object
 *      parameters:
 *        - in: path
 *          name: artistIdOrAlias
 *          description: Artist id or alias
 *          required: true
 *          type: string
 *    put:
 *      tags:
 *        - artist
 *      summary: Update artist by id or alias
 *      description: Any nested object will be ignored.
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Artist matching ID or alias
 *          schema:
 *            $ref: "#/definitions/Artist"
 *        400:
 *          description: Error occurred
 *          schema:
 *            type: object
 *        404:
 *          description: Not found
 *          schema:
 *            type: object
 *      parameters:
 *        - in: path
 *          name: artistIdOrAlias
 *          description: Artist id or alias
 *          required: true
 *          type: string
 *    delete:
 *      tags:
 *        - artist
 *      summary: Delete artist by id or alias
 *      description: ""
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Artist matching ID or alias
 *          schema:
 *            $ref: "#/definitions/Artist"
 *        400:
 *          description: Error occurred
 *          schema:
 *            type: object
 *        404:
 *          description: Not found
 *          schema:
 *            type: object
 *      parameters:
 *        - in: path
 *          name: artistIdOrAlias
 *          description: Artist id or alias
 *          required: true
 *          type: string
 *
 */

var artistRouter = express.Router(routeParams);
app.use('/artist/:id', artistRouter);
artistRouter.use(ArtistHandler.populateByAlias);
artistRouter.get('/follow', ArtistHandler.getFollower);
artistRouter.post('/follow', UserHandler.loggedInOnly, ArtistHandler.validateFeedFlags, ArtistHandler.postFollower);
artistRouter.put('/follow', ArtistHandler.validateFeedFlags, ArtistHandler.updateFollower);
artistRouter.delete('/follow', ArtistHandler.deleteFollower);
artistRouter.post('/transform_feed_item', ArtistHandler.populateFeedItem, ArtistHandler.transformFeedItem);
artistRouter.get('/feed/:type', ArtistHandler.getFeed, ArtistHandler.transformFeed);

artistRouter.post('*', UserHandler.loggedInOnly);
artistRouter.get('/', ArtistHandler.injectFollowerCount, ArtistHandler.getByAlias);
artistRouter.put('/', ArtistHandler.checkAdminPermissions, ArtistHandler.filterUpdateDocument, ArtistHandler.validate, ArtistHandler.update);
artistRouter.delete('/', ArtistHandler.checkOwnerPermissions, ArtistHandler.delete);
artistRouter.put('/owner/:id', ArtistHandler.checkOwnerPermissions, UserHandler.populateByEmail, ArtistHandler.editOwner);
artistRouter.post('/admin/:id', ArtistHandler.checkAdminPermissions, UserHandler.populateByEmail, ArtistHandler.addAdmin);
artistRouter.delete('/admin/:id', ArtistHandler.checkAdminPermissions, UserHandler.populateByEmail, ArtistHandler.removeAdmin);

artistRouter.get('/post', PostHandler.getAll('artist'));
artistRouter.post('/post', ArtistHandler.checkAdminPermissions, PostHandler.socialMediaFlags,
   PostHandler.postNew('artist'),
   ArtistHandler.addBlogToFeed,
   PostHandler.twitter('artist'),
   PostHandler.facebook('artist'));
artistRouter.get('/post/:id', PostHandler.populateById('artist'), PostHandler.getById);
artistRouter.put('/post/:id', ArtistHandler.checkAdminPermissions, PostHandler.populateById('artist'), PostHandler.update('artist'));
artistRouter.delete('/post/:id', ArtistHandler.checkAdminPermissions, PostHandler.populateById('artist'), PostHandler.delete('artist'));

artistRouter.get('/release', ReleaseHandler.nestedGetAll('artist', 'artist'));
artistRouter.get('/event', EventHandler.nestedGetAll('artist', 'linked_artists'));

// We need to figure out what members are. I figure "members" will be other artists or a string
//artistRouter.post('/member/:id', UserHandler.populateByEmail, ArtistHandler.addMember);
//artistRouter.delete('/member/:id', UserHandler.populateByEmail, ArtistHandler.removeMember);;



artistRouter.get('/twitter/authorize', ArtistHandler.checkAdminPermissions, ArtistHandler.twitterAuth);
artistRouter.post('/twitter/callback', ArtistHandler.checkAdminPermissions, ArtistHandler.twitterCallback);

artistRouter.post('/facebook/login', ArtistHandler.checkAdminPermissions, ArtistHandler.facebookLogin);

app.get('/event', EventHandler.getAll);
app.post('/event', UserHandler.loggedInOnly, EventHandler.validate, EventHandler.postNew);
app.post('/validate/event', UserHandler.loggedInOnly, EventHandler.validate, EventHandler.validateSuccess);
app.post('/validate/event/:id', UserHandler.loggedInOnly, EventHandler.populateById, EventHandler.checkAdminPermissions, EventHandler.validate, EventHandler.validateSuccess);
app.get('/event/model/:role', EventHandler.fieldMetadata);


var eventRouter = express.Router(routeParams);
app.use('/event/:id', eventRouter);
eventRouter.use(EventHandler.populateById);

eventRouter.get('/follow', EventHandler.getFollower);
eventRouter.post('/follow', UserHandler.loggedInOnly, EventHandler.validateFeedFlags, EventHandler.postFollower);
eventRouter.put('/follow', EventHandler.validateFeedFlags, EventHandler.updateFollower);
eventRouter.delete('/follow', EventHandler.deleteFollower);
eventRouter.post('/transform_feed_item', EventHandler.populateFeedItem, EventHandler.transformFeedItem);
eventRouter.get('/feed/:type', EventHandler.getFeed, EventHandler.transformFeed);

eventRouter.post('*', UserHandler.loggedInOnly);
eventRouter.get('/', EventHandler.getById);
eventRouter.put('/', EventHandler.checkAdminPermissions, EventHandler.filterUpdateDocument, EventHandler.validate, EventHandler.update);
eventRouter.delete('/', EventHandler.checkOwnerPermissions, EventHandler.delete);

eventRouter.put('/owner/:id', EventHandler.checkOwnerPermissions, UserHandler.populateById, EventHandler.editOwner);
eventRouter.post('/admin/:id', EventHandler.checkAdminPermissions, UserHandler.populateById, EventHandler.addAdmin);
eventRouter.delete('/admin/:id', EventHandler.checkAdminPermissions, UserHandler.populateById, EventHandler.removeAdmin);
eventRouter.post('/artist/:id', EventHandler.checkAdminPermissions, ArtistHandler.populateByAlias, EventHandler.addArtist);
eventRouter.delete('/artist/:id', EventHandler.checkAdminPermissions, ArtistHandler.populateByAlias, EventHandler.removeArtist);
eventRouter.post('/post', EventHandler.checkAdminPermissions, PostHandler.postNew('event'));
eventRouter.put('/post/:id', EventHandler.checkAdminPermissions, PostHandler.populateById('event'), PostHandler.update('event'));
eventRouter.delete('/post/:id', EventHandler.checkAdminPermissions, PostHandler.populateById('event'), PostHandler.delete('event'));

/*
 * All POST routes below here need a user to be logged in
 */
app.post('*', UserHandler.loggedInOnly);

app.get('/release', ReleaseHandler.getAll);
app.post('/release', ReleaseHandler.validate, ReleaseHandler.postNew);
app.post('/validate/release', ReleaseHandler.validate, ReleaseHandler.validateSuccess);
app.post('/validate/release/:id', ReleaseHandler.populateById, ReleaseHandler.checkAdminPermissions, ReleaseHandler.validate, ReleaseHandler.validateSuccess);
app.get('/artist/model/:role', ReleaseHandler.fieldMetadata);

var releaseRouter = express.Router(routeParams);
app.use('/release/:id', releaseRouter);
releaseRouter.use(ReleaseHandler.populateById);
releaseRouter.get('/', ReleaseHandler.getById);
releaseRouter.put('/', ReleaseHandler.checkAdminPermissions, ReleaseHandler.filterUpdateDocument, ReleaseHandler.validate, ReleaseHandler.update);
releaseRouter.delete('/', ReleaseHandler.checkOwnerPermissions, ReleaseHandler.delete);

releaseRouter.post('/song', ReleaseHandler.checkAdminPermissions, ReleaseHandler.addSong);
releaseRouter.delete('/song/:id', ReleaseHandler.checkAdminPermissions, ReleaseHandler.populateSong, ReleaseHandler.removeSong);
releaseRouter.put('/owner/:owner_id', ReleaseHandler.checkOwnerPermissions, ReleaseHandler.editOwner);
releaseRouter.post('/admin/:admin_id', ReleaseHandler.checkAdminPermissions, ReleaseHandler.addAdmin);
releaseRouter.delete('/admin/:admin_id', ReleaseHandler.checkAdminPermissions, ReleaseHandler.removeAdmin);
releaseRouter.post('/member/:artist_id', ReleaseHandler.checkAdminPermissions, ReleaseHandler.addArtist);
releaseRouter.delete('/member/:artist_id', ReleaseHandler.checkAdminPermissions, ReleaseHandler.removeArtist);

app.get('/request', RequestHandler.getAll);
app.post('/request', RequestHandler.validate, RequestHandler.postNew);
app.post('/validate/request', RequestHandler.validate, RequestHandler.validateSuccess);
app.get('/request/model/:role', RequestHandler.fieldMetadata);

app.get('/search/artist', SearchHandler.artistSearch);
app.get('/search/event', SearchHandler.eventSearch);
