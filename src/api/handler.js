var _ = require('underscore'),
   Promise = require('bluebird');

var constants = require('../constants.js'),
   Artist = require('../mongo/artist.js'),
   ArtistCtrl = require('../controllers/artist.js'),
   Event = require('../mongo/event.js'),
   EventCtrl = require('../controllers/event.js'),
   Release = require('../mongo/release.js'),
   Post = require('../mongo/post.js'),
   feed_constants = constants.feeds,
   UserError = require('../error').UserError,
   sendError = require('../util/error.js').sendError;

function filteredResponseBuilder(user, res, ctrl) {
   return function filteredResponse(err, data) {
      if (err) {
         return sendError(err, res);
      } else {
         res.json(ctrl.filterDocument(data, user));
      }
   };
}

function basicResponseBuilder(res) {
   return function basicResponse(err, data) {
      if (err) return res.status(400).json(err);
      if (!data) return res.status(404).json({
         error: 'object not found'
      });
      res.json(data);
   };
}

module.exports = function (ctrl) {
   var type = ctrl.model.name;

   return {
      getAll: function getAllHandler(req, res, next) {
         var {query, sort, page} = req.query;
         
         try {
            query = JSON.parse(query);
         } catch(e) {
            query = {};
         }

         if(sort) {
            try {
               sort = JSON.parse(sort);
            } catch(e) {
               sort = sort || "";
            }
         }

         ctrl.getAll(query, sort, page, filteredResponseBuilder(req.user, res, ctrl));
      },
      getById: function getByIdHandler(req, res, next) {
         res.json(req.populated[type]);
      },
      getByAlias: function getByAliasHandler(req, res, next) {
         res.json(req.populated[type]);
      },
      nestedGetAll: function nestedGetAll(parent, field) {
         return function (req, res, next) {
            var {query, page, sort} = req.query;

            try {
               query = JSON.parse(query);
            } catch(e) {
               query = {};
            }

            query[field] = req.populated[parent]._id;
            sort = sort || {};

            ctrl.getAll(query, sort, page, filteredResponseBuilder(req.user, res, ctrl));
         };
      },
      postNew: function postNewHandler(req, res, next) {
         req.body.owner = req.user;
         ctrl.postNew(req.body, filteredResponseBuilder(req.user, res, ctrl));
      },
      update: function updateHandler(req, res, next) {
         ctrl.updateDocument(
            req.body,
            req.populated[type],
            filteredResponseBuilder(req.user, res, ctrl)
         );
      },
      delete: function deleteHandler(req, res, next) {
         ctrl.deleteDocument(req.populated[type], filteredResponseBuilder(req.user, res, ctrl));
      },
      populateById: function populateById(req, res, next) {
         ctrl.getById(req.params.id, function (err, mongoDoc) {
            if (err) return sendError(err, res);

            req.populated[type] = mongoDoc;
            next();
         });
      },
      populateByAlias: function populateByAlias(req, res, next) {
         ctrl.getByIdOr('alias', req.params.id, function (err, mongoDoc) {
            if (err) return sendError(err, res);

            req.populated[type] = mongoDoc;
            next();
         });
      },
      checkAdminPermissions: function checkAdminPermissions(req, res, next) {
         var errors = ctrl.checkAdminPermissions(req.body, req.populated[type], req.user);
         if (!errors) return next();

         res.status(403).json({
            errors: errors
         });
      },
      checkOwnerPermissions: function checkOwnerPermissions(req, res, next) {
         var errors = ctrl.checkOwnerPermissions(req.body, req.populated[type], req.user);
         if (!errors) return next();

         res.status(403).json({
            errors: errors
         });
      },
      filterUpdateDocument: function filterUpdateDocument(req, res, next) {
         ctrl.filterUpdateDocument(req.body, req.populated[type], req.user);
         next();
      },
      validate: function validate(req, res, next) {
         var model;

         if (req.params.id) {
            model = req.populated[type];
         } else {
            model = new ctrl.Mongo();
         }

         ctrl.validate(req.body, model, req.user, function (err) {
            if (err) return res.status(400).send(err);
            next();
         });
      },
      validateSuccess: function validateSuccess(req, res) {
         res.send({});
      },
      fieldMetadata: function fieldMetadata(req, res) {
         var model = ctrl.model.permissions[req.params.role];
         res.json(model);
      },

      /*
       * Feed and Follow Module
       */
      getFollower: function getFollower(req, res, next) {
         // TODO: Do not require user to exist when getting follow status
         if (!req.user || req.user.id === constants.NO_USER) return basicResponseBuilder(res)({
            error: 'no user'
         });
         ctrl.getFollower(req.populated[type], req.user, basicResponseBuilder(res));
      },

      postFollower: function postFollower(req, res, next) {
         ctrl.postFollower(req.body.flags, req.populated[type], req.user, basicResponseBuilder(res));
      },

      updateFollower: function updateFollower(req, res, next) {
         var callback = filteredResponseBuilder(req.user, res, ctrl);
         ctrl.updateFollower(req.body.flags, req.populated[type], req.user, callback);
      },

      deleteFollower: function deleteFollower(req, res, next) {
         var callback = filteredResponseBuilder(req.user, res, ctrl);
         ctrl.deleteFollower(req.populated[type], req.user, callback);
      },

      validateFeedFlags: function validateFeedFlags(req, res, next) {
         var i, feed, flags = req.body.flags;

         if (!_.isArray(flags)) flags = [];

         if (flags.length > feed_constants[type].length) {
            return sendError(new UserError('Too many feed flags'), res);
         }

         for (i = 0; i < flags.length; i++) {
            feed = flags[i];
            if (!feed_constants[type][feed]) {
               return sendError(new UserError('Feed type ' + feed + ' does not exist'), res);
            }
         }

         req.body.flags = flags;
         next();
      },

      getFeed: function getFeed(req, res, next) {
         var options = {
            limit: 25
         };

         if (req.query.limit && typeof req.query.limit === 'number') {
            if (req.query.limit > 0 && req.query.limit <= 25) {
               options.limit = req.query.limit;
            }
         }

         if (req.query.id_lt) {
            options.id_lt = req.query.id_lt;
         }

         if (req.query.mark_read) {
            options.mark_read = req.query.mark_read;
         }

         if (req.query.mark_seen) {
            options.mark_seen = req.query.mark_seen;
         }

         ctrl.getFeed(req.params.type, req.populated[type], options, function (err, feed) {
            if (err) return res.status(500).json(err);

            req.populated.feed = feed;
            next();
         });
      },

      populateFeedItem: function populateFeedItem(req, res, next) {
         let actorP, objectP;

         switch (req.body.actor_type) {
         case 'artist': 
            actorP = Artist.findById(req.body.actor);
            break;
         case 'event':
            actorP = Event.findById(req.body.actor);
            break;
         default:
            return res.status(400).json({error: "Unknown actor type"});
         }

         switch (req.body.object_type) {
         case 'artist': 
            objectP = Artist.findById(req.body.object);
            break;
         case 'event':
            objectP = Event.findById(req.body.object);
            break;
         case 'release':
            objectP = Release.findById(req.body.object);
            break;
         case 'post':
            objectP = Post.findById(req.body.object);
            break;
         case 'profile':
         case 'future_events':
            break;
         default:
            return res.status(400).json({error: "Unknown object type"});
         }

         actorP = actorP.then(actor => {
            req.body.actor = actor;
         });

         if (objectP) {
            objectP = objectP.then(object => {
               req.body.object = object;
            });
         } else {
            objectP = Promise.resolve();
         }

         Promise.all([actorP, objectP])
         .then(() => next())
         .catch(error => res.status(500).json({error}));
      },

      transformFeedItem: function transformFeedItem(req, res) {
         let ctrl, feedItem;

         switch (req.body.actor_type) {
         case 'artist': 
            ctrl = ArtistCtrl;
            break;
         case 'event':
            ctrl = EventCtrl;
            break;
         default:
            return res.status(500).json({error: 'Unknown feed type. Should have been caught'});
         }

         if(req.body.activities) {
            feedItem = ctrl.transformAggregatedFeedItem(req.body, req.user);
         } else {
            feedItem = ctrl.transformFeedItem(req.body, req.user);
         }

         res.json(feedItem);
      },

      transformFeed: function transformFeed(req, res) {
         req.populated.feed.results = req.populated.feed.results.map(item => {
            if(item.activities && item.activities.length > 0) {
               let activity = item.activities[0];
               
               item.actor = activity.actor;
               item.actor_type = activity.actor_type;
               item.object = activity.object;
               item.object_type = activity.object_type;
            }

            return item;
         });

         res.json(req.populated.feed);
      }
   };
};
