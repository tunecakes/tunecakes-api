/**
 * TUNECAKES API SERVER
 */
require('newrelic');

var bodyParser = require('body-parser'),
   cors = require('cors'),
   config = require('config'),
   logging = require('./logging'),
   log = logging.logger,
   express = require('express'),
   mongoose = require('mongoose'),
   passport = require('passport'),
   swagger = require('swagger-express'),
   api = require('./api');

var app = express();
var port = process.env.PORT ||  config.get("api.port");

mongoose.connection.once('open', function() {
    log.info('MongoDB event open');
    log.debug('MongoDB connected [%s]', config.get('db'));

    mongoose.connection.on('connected', function() {
        log.info('MongoDB event connected');
    });

    mongoose.connection.on('disconnected', function() {
        log.warn('MongoDB event disconnected');
    });

    mongoose.connection.on('reconnected', function() {
        log.info('MongoDB event reconnected');
    });

    mongoose.connection.on('error', function(err) {
        log.error('MongoDB event error: ' + err);
    });
});

mongoose.connect(config.get('db'), function(err) {
    if (err) {
        log.error('MongoDB connection error: ' + err);
        process.exit(1);
    }
});

// Use bluebird for promises
mongoose.Promise = require('bluebird');

if(config.trust_proxy) {
   app.enable('trust proxy');
}

// TODO: figure out cors details for various envs
app.use(cors(config.get('cors')));
app.use(bodyParser.urlencoded({
   extended: true
}));
app.use(bodyParser.json({
   limit: '128kb'
}));
app.use(passport.initialize());
app.use(logging.middleware);
app.use('/api', api);

app.use(swagger.init(app, {
   apiVersion: '1.0',
   swaggerVersion: '2.0',
   swaggerURL: '/docs',
   swaggerJSON: '/swagger.json',
   swaggerUI: config.get("distribution_directory") + '/swagger/',
   basePath: '/api',
   info: {
      title: 'TuneCakes API',
      description: 'REST endpoints for TuneCakes resources'
   },
   apis: ['./src/api/index.js'],
   app: api,
   mongoose: mongoose
}));

if (config.has('update_getstream') && config.get("update_getstream"))
   require('./util/getstream').repopulateFollowStreams();

log.info('listening on port ' + port + " and ip " + config.get("api.host"));
app.listen(port, config.get("api.host"));

// Start reminder cron
require('./util/cron.js');

// Create default user if it doesn't exist
require('./util/default_user.js').ensureDefaultUser();

module.exports = app;
