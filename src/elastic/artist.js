var index = require('../models/artist.js').name;

var settings = {
   settings : {
      analysis: {
         filter: {
            edge_ngram_filter: {
               type: 'edgeNGram',
               min_gram: 2,
               max_gram: 20
            }
         },
         analyzer: {
            artist_name: {
               type: 'custom',
               tokenizer: 'whitespace',
               filter: [
                  'lowercase',
                  'edge_ngram_filter'
               ]
            }
         },
      }
   }
};

var properties = {
   // Analyzed fields
   name: {
      type: 'string',
      store: true,
      analyzer: 'artist_name'
   },
   start_date: {
      type: 'date',
      store: true
   },
   location: {
      type: 'geo_point',
      store: true
   },
   tags: {
      type: 'keyword',
      store: true
   },

   // Stored but not analyzed fields
   id: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   description: {
      type: 'string',
      index: 'not_analyzed'
   },
   image_link: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   city: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   state: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   country: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   zip: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   }
};

module.exports = {
   index: index,
   type: index,
   settings: settings,
   properties: properties
};
