var index = require('../models/event.js').name;

var settings = {
   settings : {
      analysis: {
         filter: {
            edge_ngram_filter: {
               type: 'edgeNGram',
               min_gram: 2,
               max_gram: 20
            }
         },
         analyzer: {
            artist_name: {
               type: 'custom',
               tokenizer: 'whitespace',
               filter: [
                  'lowercase',
                  'edge_ngram_filter'
               ]
            }
         },
      }
   }
};

var properties = {
   // Analyzed fields
   name: {
      type: 'string',
      store: true,
      analyzer: 'standard'
   },
   unlinked_artists: {
      type: 'nested',
      properties: {
         name: {
            store: true,
            type: 'string',
            analyzer: 'artist_name'
         }
      }
   },
   linked_artists: {
      type: 'nested',
      properties: {
         name: {
            store: true,
            type: 'string',
            analyzer: 'artist_name'
         },
         id: {
            store: true,
            type: 'string',
            index: 'not_analyzed'
         },
         alias: {
            store: true,
            type: 'string',
            index: 'not_analyzed'
         },
         tags: {
            type: 'keyword',
            store: true
         }
      }
   },
   start_date: {
      type: 'date',
      store: true
   },
   end_date: {
      type: 'date',
      store: true
   },
   location: {
      type: 'geo_point',
      store: true
   },
   tags: {
      type: 'keyword',
      store: true,
   },

   // Stored but not analyzed fields
   id: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   description: {
      type: 'string',
      index: 'not_analyzed'
   },
   image_link: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   type: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   venue_name: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   city: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   state: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   country: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   },
   zip: {
      type: 'string',
      store: true,
      index: 'not_analyzed'
   }
};

module.exports = {
   index: index,
   type: index,
   settings: settings,
   properties: properties
};
