var config = require('./config')
var requestHandler = require('./request-handler'),
   log = require('../logging.js').logger;

var PATH = '/3/image';
var IMAGE_FORMAT = 'base64';

var images = {
   get: function (imageId, callback) {
      requestHandler.getRequest(PATH + '/' + imageId, callback);
   },
   post: function (base64Image, callback) {
      var postData = {
         image: base64Image,
         album: config.albumId,
         type: IMAGE_FORMAT
      };



      requestHandler.postRequest(PATH, postData, function (json) {
         log.info('Imgur response: ');
         log.info(json);

         if (!json.success) {
            var err = {
               'type': 'IMGUR REQUEST ERROR',
               'message': json.data.error
            };
            return callback(null, err);
         }

         callback(json.data.link);
      });
   }
};

module.exports = images;
