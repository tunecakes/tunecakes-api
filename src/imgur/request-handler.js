var https = require('https');
var qs = require('querystring');
var config = require('./config');
var HOST = 'api.imgur.com';
var POST = 'POST';
var GET = 'GET';
var PORT = 443;
var TIMEOUT_BUFFER = 60;
var REFRESH_POSTDATA = JSON.stringify({
   refresh_token: config.refreshToken,
   client_id: config.appId,
   client_secret: config.appSecret,
   grant_type: 'refresh_token'
});

var log = require('../logging.js').logger;

var _getAccessToken = function (callback) {
   // make this more async, only wait on _refreshToken for callback if we NEED
   // otherwise, just refresh the token asynchronously as needed
   if (config.accessToken == null || config.accessTokenTimeout < new Date()) {
      _refreshToken(callback)
   } else {
      callback(config.accessToken);
   }
};


var _refreshToken = function (callback) {
   var options = {
      'host': HOST,
      'path': '/oauth2/token',
      'method': POST,
      'port': PORT,
      'headers': {
         'Content-Type': 'application/json',
         'Content-Length': REFRESH_POSTDATA.length
      }
   }

   log.info('Refresh Token Request');
   log.info(REFRESH_POSTDATA);
   var request = https.request(options, function (res) {
      res.setEncoding('utf8');

      var data = '';
      res.on('data', function (chunk) {
         data += chunk;
      });
      res.on('end', function () {
         log.info('Refresh Token Response');
         log.info(data);

         var json = JSON.parse(data);
         timeout = new Date();
         timeout = timeout.setSeconds(timeout.getSeconds() + json.expires_in - TIMEOUT_BUFFER);
         config.accessTokenTimeout = timeout;

         config.accessToken = json.access_token;
         callback(config.accessToken);
      });
   });

   request.write(REFRESH_POSTDATA);
   request.end();

};

var requestHandler = {
   getRequest: function (path, callback) {
      var options = {
         host: HOST,
         path: path,
         method: GET,
         port: PORT,
         headers: {
            'Content-Type': 'application/json'
         }
      };

      var request = https.request(options, function (res) {
         res.setEncoding('utf8');

         var data = '';
         res.on('data', function (chunk) {
            data += chunk;
         });

         res.on('end', function () {
            var json = JSON.parse(data);
            callback(json);
         });
      });

      request.end();
   },
   postRequest: function (path, postData, callback) {
      log.info('Preparing Imgur POST Request');

      var data = JSON.stringify(postData);
      var options = {
         host: HOST,
         path: path,
         method: POST,
         port: PORT,
         headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
         }
      };

      _getAccessToken(function (token) {
         options.headers.Authorization = 'Bearer ' + token;

         log.info('Sending Imgur POST Request');
         log.info(options);

         var request = https.request(options, function (res) {
            res.setEncoding('utf8');

            var data = '';
            res.on('data', function (chunk) {
               data += chunk;
            });
            res.on('end', function () {
               var json = JSON.parse(data);
               callback(json);
            });
         });

         request.write(data);
         request.end();
      });
   }
};

module.exports = requestHandler;
