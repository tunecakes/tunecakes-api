var env = process.env.NODE_ENV || 'local';

var config = {
   appId: '4b3219e90bf42ab',
   appSecret: 'e7785be0b92c614c24ea2f899b67cfb7d0665367',
   refreshToken: '399c449ed9feccf5fa0dc6c41ad41edc4db2febc',
   accessToken: null,
   accessTokenTimeout: null
}

switch(env) {
   case 'LOCAL':
      config.albumId = 'x0PpE';
      break;
   case 'STAGING':
      config.albumId = 'QyfVC';
      break;
   case 'PRODUCTION':
      config.albumId = 'L3xwF';
      break;
}

module.exports = config;