var config = require('config'),
   request = require('request-promise'),
   qs = require('querystring'),
   log = require('../logging.js').logger;

var twitterApi = 'https://api.twitter.com',
   accessTokenUrl = twitterApi + '/oauth/access_token',
   requestTokenUrl = twitterApi + '/oauth/request_token',
   tweetUrl = twitterApi + '/1.1/statuses/update.json',
   authUrl = twitterApi + '/oauth/authorize';

var consumerKey = config.twitter.clientID,
   consumerSecret = config.twitter.clientSecret;

/*
 * Example data on success:
 * { oauth_token: 'yeEA1QAAAAAAxiWFAAABV_SdI18',
 *   oauth_token_secret: 'noWDWk7FmX8cNxedmb3gulmXRRtk8t9m',
 *   oauth_callback_confirmed: 'true' }
 */
function getOAuthRequestToken(callbackUrl) {
   var oauth = {
      callback: callbackUrl,
      consumer_key: consumerKey,
      consumer_secret: consumerSecret
   };
   return request.post({url: requestTokenUrl, oauth: oauth})
   .then(function parseOAuth(body) {
      return qs.parse(body);
   });
}

function getAuthorizationUrl(oauthToken) {
   return authUrl + '?oauth_token=' + oauthToken;
}

function parseVerifierToken(verifier, token) {
   var oauth = {
      token: token,
      verifier: verifier,
      consumer_key: consumerKey,
      consumer_secret: consumerSecret
   };

   log.info('Sending Twitter verifier', verifier, token);

   return request.post({url: accessTokenUrl, oauth: oauth})
   .then(function parseAccessToken(body) {
      return qs.parse(body);
   });
}

function tweet(message, token, tokenSecret) {
   var oauth = {
      token: token,
      token_secret: tokenSecret,
      consumer_key: consumerKey,
      consumer_secret: consumerSecret
   };

   return request.post({
      url: tweetUrl,
      oauth: oauth,
      qs: { status: message }
   });
}

module.exports = {
   getOAuthRequestToken: getOAuthRequestToken,
   getAuthorizationUrl: getAuthorizationUrl,
   parseVerifierToken: parseVerifierToken,
   tweet: tweet
}
