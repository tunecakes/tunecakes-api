var InstantError = require('./_base').InstantError;

function AuthError() {
   InstantError.apply(this, arguments);
   this.httpCode(401);
   //InstantError.prototype.httpCode.call(this, 401);
}

AuthError.prototype = Object.create(InstantError.prototype);

module.exports = AuthError;
