function InstantError() {
   var args = Array.prototype.slice.call(arguments);

   this.status = 500;
   this.errors = args;
}

InstantError.prototype = {
   httpCode: function status(code) {
      if (code) this.status = code;
      return this.status;
   },
   httpResponse: function () {
      return {
         errors: this.errors
      };
   },
   addError: function addError() {
      var args = Array.prototype.slice.call(arguments);
      for (var i = 0; i < args.length; i++) {
         this.errors.push(args[i]);
      }
   },
   hasErrors: function hasErrors() {
      return this.errors.length > 0;
   }
};

function deferredError(user) {
   this.user = user;
   this.errors = [];
}

module.exports = {
   InstantError: InstantError,
   deferredError: deferredError
};