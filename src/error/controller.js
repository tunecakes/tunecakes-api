var InstantError = require('./_base').InstantError;

function ControllerError() {
   InstantError.apply(this, arguments);
   this.httpCode(404);
   //InstantError.prototype.httpCode.call(this, 401);
}

ControllerError.prototype = Object.create(InstantError.prototype);

module.exports = ControllerError;
