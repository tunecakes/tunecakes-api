module.exports = {
   AuthError: require('./auth_error'),
   MongoError: require('./mongo_error'),
   UserError: require('./user_error'),
   ControllerError: require('./controller.js')
};
