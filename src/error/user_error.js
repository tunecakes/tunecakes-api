var InstantError = require('./_base').InstantError;

function UserError(message) {
   InstantError.apply(this, arguments);
   this.httpCode(403);
}

UserError.prototype = Object.create(InstantError.prototype);

module.exports = UserError;
