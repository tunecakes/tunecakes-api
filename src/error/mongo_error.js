var async = require('async');
var util = require('util');
var _ = require('underscore');

var InstantError = require('./_base').InstantError,
   log = require('../logging.js').logger;

var TYPE = 'MongoError';
var UNKNOWN_ERROR = 'Unknown error response from Mongo.';
var REQUIRED_ERROR = 'Required field "%s" is missing.';
var VALIDATION_ERROR = 'Field "%s" is improperly formatted.';
var UNIQUE_ERROR = 'Field "%s" is not unique.';
var CAST_ERROR = 'Unable to cast "%s".';
var DB_ERROR = 'DB error. Need to figure out what the types of errors are.';
var DOC_NOT_FOUND_ERROR = 'No document found with the info given.';
var WRITE_PERM_ERROR = 'You do not have permission to edit one or more of these properties.';


var MongoError = {};

MongoError.parseDbError = function (error, callback) {
   log.error('Unable to parse Mongo DB Error', error);
   var iError = new InstantError(DB_ERROR);
   iError.httpCode(500);
   return iError;
};

MongoError.parseValidationErrors = function (error, callback) {
   var iError;

   // Validation errors come in as an object with each key being the field
   // where validation failed. Map them to an array if that's the case
   error.errors = _.isArray(error.errors) ? error.errors : _.map(error.errors, function (val, key) {
      return val
   });

   for (var i = 0; i < error.errors.length; i++) {
      var message;
      var value = error.errors[i];

      if (value.name == 'CastError')
         return this.parseCastError(value);

      switch (value.kind) {
         case 'required':
            message = util.format(REQUIRED_ERROR, value.path);
            break;
         case 'user defined':
            message = util.format(VALIDATION_ERROR, value.path);
            message = message + ' ' + value;
            break;
         case 'unique':
            message = util.format(VALIDATION_ERROR, value.path);
            break;
         default:
            log.error('Unable to parse Mongo Validation Error.', JSON.stringify(error));
            message = UNKNOWN_ERROR;
            break;
      }

      if (!iError) {
         iError = new InstantError(message);
      } else {
         iError.addError(message);
      }
   }

   iError.httpCode(400);
   return iError;
};

MongoError.parseCastError = function (error) {
   var iError = new InstantError(util.format(CAST_ERROR, error.path));
   iError.httpCode(400);
   return iError;
};

MongoError.parseError = function (error) {
   if (error.name === 'ValidationError') {
      return this.parseValidationErrors(error);
   } else if (error.name === 'ValidatorError') {
      return;
   } else if (error.name == 'MongoError') {
      return this.parseDbError(error);
   } else if (error.name == 'CastError') {
      return this.parseCastError(error);
   } else {
      var iError = new InstantError(message);
      var message = error.message || 'MongoError failed to parse the errors.';
      log.error(message, error);
      iError.httpCode(500);
      return iError;
   }
};

MongoError.writePermissionError = function (err) {
   var iError = new InstantError(WRITE_PERM_ERROR);
   iError.httpCode(401);
   return iError;
};

module.exports = MongoError;
