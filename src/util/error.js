//module.exports = exports;
var log = require('../logging.js').logger;

exports.clientError = function (req, res, next) {
	res.send(404, 'THIS IS A 404 ERROR');
};

exports.serverError = function(err, req, res, next) {
	log.info(err.stack);
	res.send(200, 'THERE HAS BEEN A SERVER ERROR');
};

exports.sendError = function sendError(err, res) {
   if (typeof err == 'string') {
      // errors shouldn't be strings, but just in case
      return res.status(400).json(err);
   } else {
      if (err.httpCode) {
         return res.status(err.httpCode()).json(err.httpResponse());
      } else {
         // Node errors should be wrapped in a tuncakes error, but just in case
         return res.status(400).json({
            errors: [err.message]
         });
      }
   }
};
