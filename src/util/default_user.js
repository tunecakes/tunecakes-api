var config = require('config'),
   log = require('../logging.js').logger,
   uc = require('../controllers/user.js');

exports.ensureDefaultUser = function () {
   uc.getBy('email', config.default_user.email, function (err, user) {
      if (user) {
         exports.defaultUserId = user.id;
         return log.info('Default user already exists');
      }

      uc.postNew(config.default_user, function (err, user) {
         if (err) return log.warn('Failed to create default user', err);

         exports.defaultUserId = user.id;
         log.info('Default user created, please change password.', config.default_user);
      });
   });
};

exports.defaultUserId = null;
