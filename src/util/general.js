var _ = require('underscore'),
   mongoose = require('mongoose'),
   ObjectId = mongoose.Types.ObjectId;

function isObjectId(str) {
   return ObjectId.isValid(str);
}

function removeReferences(model, field, id, cb) {
   var to_delete = {};
   to_delete[field] = id;

   mongoose.model(model).update(to_delete, {
         $pull: to_delete
      }, {
         multi: true
      },
      function (err, artist) {
         cb();
      });
}

module.exports = {
   isObjectId: isObjectId,
   removeReferences: removeReferences
};
