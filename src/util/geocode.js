var mongoose = require('mongoose'),
   config = require('config'),
   log = require('../logging.js').logger,
   request = require('request-promise');

var Schema = mongoose.Schema,
   mappingSchema = new Schema({
      zip: { type: String, index: true, unique: true, required: true },
      location: {
         lat: { type: Number, required: true },
         lon: { type: Number, required: true } 
      }
   }),
   GeoMapping = mongoose.model('GeoMapping', mappingSchema);

function zipToGeo(zip, callback) {
   var mongoP, googleP;
      
   mongoP = mongoZipToGeo(zip);

   mongoP.then(function(doc) {
      if(!doc) {
         log.debug('Geo mapping document DNE in mongo DB', zip);
         throw 'Document not found';
      }

      log.debug('Found mongo GeoMapping doc', doc);

      callback(null, doc.location);
   }).catch(function(err) {
      return googleZipToGeo(zip)
      .then(function(location) {
         storeZipGeoMapping(zip, location);
         callback(null, location);
      }, callback);
   });
}

function mongoZipToGeo(zip) {
   return GeoMapping.findOne({ zip: zip });
}

function googleZipToGeo(address) {
   var req = {
      url: 'https://maps.googleapis.com/maps/api/geocode/json',
      qs: {address: address, key: config.google.apiKey},
      json: true
   };

   log.info('Retrieving lat/lon for address info', address);

   return request(req)
   .then(function(data) {
      var lat, lon, res;

      log.debug('Google geocode response', data);

      if(data.status !== 'OK') throw data;
      else if(!data.results || data.results.length === 0) throw data;
      else if(data.results.length > 1)
         log.warn('Multiple results return from Google Geo API', data);

      res = data.results[0];

      if(!res.geometry || !res.geometry.location) throw data;

      return {
         lat: res.geometry.location.lat,
         lon: res.geometry.location.lng
      };
   });
}

function storeZipGeoMapping(address, location) {
   var map = new GeoMapping({
      address: address,
      location: location
   });

   log.debug('Storing new GeoMapping in mongo', map.toJSON());

   map.save();
}

module.exports = {
   zipToGeo: zipToGeo,
   mongoZipToGeo: mongoZipToGeo,
   googleZipToGeo: googleZipToGeo,
   storeZipGeoMapping: storeZipGeoMapping
};
