var async = require('async'),
   config = require('config'),
   CronJob = require('cron').CronJob,
   Reminder = require('../mongo/reminder.js').active,
   log = require('../logging.js').logger,
   InactiveReminder = require('../mongo/reminder.js').inactive;

var activeJob;

new CronJob(config.reminder_cron, function() {
   var now = new Date();
   
   log.info('Running reminder cron...', now);
   
   Reminder.find({remind_date: {'$lt': now}}, function(err, docs) {
      if(err) return log.error('Error occurred getting reminders', err);
      

      async.each(docs, function(doc, cb) {
         // send activitiy
         // send email
         doc.remove();
         cb();
      }, function() {
         log.info('Finished cron', now);
      });
   });
}, null, true);


