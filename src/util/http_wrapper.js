function post(queryString, HOST, PATH, callback) {

   var options = {
      host: HOST,
      port: 80,
      path: PATH,
      method: 'POST',
      headers: {
         'Content-Type': 'application/x-www-form-urlencoded',
         'Content-Length': queryString.length
      }
   };

   var request = http.request(options, callback);

   request.write(queryString);
   request.end();
};