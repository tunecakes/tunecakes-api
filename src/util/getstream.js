var stream = require('getstream'),
   config = require('config'),
   log = require('../logging.js').logger,
   mongoFeedItem = require('../mongo/feed_item.js'),
   feed_constants = require('../constants.js').feeds;

var client = stream.connect(
   config.get("getstream.clientKey"),
   config.get("getstream.clientSecret"),
   config.get("getstream.clientID")
);

var getstream = {
   /*
    * getstream client
    */
   client: client,

   follow: function follow(followerFeed, publisherFeed) {
      followerFeed.follow(publisherFeed.slug, publisherFeed.userId, {
            limit: 0
         })
         .then(function () {
            log.debug('Getstream follow succeeded',
               followerFeed.id, publisherFeed.id);
         })
         .catch(function (err) {
            log.error('Failed to follow getstream feed',
               followerFeed.id, publisherFeed.id);
         });
   },

   followStream: function followStream(followerType, followerId, streamType, streamId) {
      var feed = client.feed(followerType, followerId);
      feed.follow(streamType, streamId, {
            limit: 0
         })
         .then(function () {
            log.debug('Getstream follow succeeded',
               followerType, followerId, streamType, streamId);
         })
         .catch(function (err) {
            log.error('Failed to follow getstream feed',
               followerType, followerId, streamType, streamId, err.error);
         });
   },

   followMany: function follow(followerFeed, feeds) {
      var follows = [];

      feeds.forEach(function (feed) {
         follows.push({
            source: followerFeed.id,
            target: feed.id
         });
      });

      return client.followMany(follows);
   },

   unfollow: function unfollow(followerFeed, publisherFeed) {
      followerFeed.unfollow(publisherFeed.slug, publisherFeed.userId, {
            keepHistory: true
         })
         .then(function () {
            log.debug('Getstream unfollow succeeded',
               followerFeed.id, publisherFeed.id);
         })
         .catch(function (err) {
            log.error('Failed to unfollow getstream feed',
               followerFeed.id, publisherFeed.id);
         });
   },

   unfollowStream: function unfollowStream(followerType, followerId, streamType, streamId) {
      var feed = client.feed(followerType, followerId);
      feed.unfollow(streamType, streamId, {
            keepHistory: true
         })
         .then(function () {
            log.debug('Getstream unfollow succeeded',
               followerType, followerId, streamType, streamId);
         })
         .catch(function (err) {
            log.error('Failed to stop following getstream feed',
               followerType, followerId, streamType, streamId, err.error);
         });

   },

   deleteFollowStream: function deleteFollowStream(user_id, object_id, feeds) {
      var notification_feed = client.feed(feed_constants.user.notifications, user_id);
      var agg_feed = client.feed(feed_constants.user.aggregated, user_id);

      for (var i = 0; i < feeds.length; i++) {
         //TODO: use getstream batch calls
         agg_feed.unfollow(feeds[i], object_id);
         notification_feed.unfollow(feeds[i], object_id);
      }
   },

   addActivity: function addActivity(activity, feed) {
      var p = feed.addActivity(activity);

      for (let i = 0; i < 3; i++) {
         p = p.catch(function () {
            return feed.addActivity(activity);
         });
      }

      p.then(function (data) {
         mongoFeedItem.create({
            feed: feed.id,
            actor: data.actor,
            actor_type: data.actor_type,
            object: data.object,
            object_type: data.object_type,
            object_data: data.object_data,
            verb: data.verb,
            time: data.time,
            getstream_id: data.id
         });
      }).catch(function (err) {
         log.error('Failed to add activity to feed', activity, feed.id, err);
      });

      return p;
   },

   countFeedItemsFromNow: function countFeedItemsFromNow(feed) {
      return mongoFeedItem.count({
         feed: feed.id,
         time: {
            $gt: new Date()
         }
      });
   },

   countFeedItemsFromDate: function countFeedItemsFromDate(feed, date) {
      return mongoFeedItem.count({
         feed: feed.id,
         time: {
            $gt: date
         }
      });
   },

   /**
    * Get all feed activities from feed_name, feed_id with options.
    *
    * @param {string} feed_name
    * @param {string} feed_id
    * @param {Object} options
    * @param {responseCallback} callback
    */
   getFeedActivities: function getFeedActivities(feed_name, feed_id, options, callback) {
      client.feed(feed_name, feed_id).get(options, function (err, res, body) {
         callback(err, body);
      });
   },

   /**
    * Repopulate all existing FollowMaps
    */
   repopulateFollowStreams: function repopulateFollowStreams() {
      throw new Error('Fix this method before you use it');

      //function followStreamUpdater(followMap) {

      /* user is undefined below, commenting this out for now */

      //_.mapObject(feed_constants[followMap.publisher_type], function(feed, flag) {
      //   getstream.followStream(feed_constants.user.aggregated,
      //                          user.id,
      //                          feed,
      //                          followMap.publisher);
      //});

      //followMap.flags.forEach(function(flag) {
      //   getstream.followStream(feed_constants.user.notifications,
      //                          followMap.follower,
      //                          feed_constants[followMap.publisher_type][flag],
      //                          followMap.publisher);
      //});

      //}

      //FollowMapMongo.find().exec(function (err, followMaps) {
      //   async.each(followMaps, followStreamUpdater);
      //});
   },

   createActivity: function createActivity(verb, actorModel, objectData, objectType) {
      var activity = {
         verb: verb,
         actor: actorModel.id,
         actor_type: actorModel._type,
      };

      if (objectType) {
         // This is typically the actor updating itself
         // We set the activity.object to objectType so it can get aggregated
         activity.object = objectType;
         activity.object_data = objectData;
         activity.object_type = objectType;
      } else {
         // The object should be a mongoose object here
         activity.object = objectData.id;
         activity.object_type = objectData._type;
      }

      return activity;
   },

   getFeed: function getFeed(feed_name, feed_id) {
      return client.feed(feed_name, feed_id);
   },

   addActivityToManyFeeds: function addActivityToManyFeeds(activity, feeds) {
      var feed_ids = [];

      feeds.forEach(function (feed) {
         feed_ids.push(feed.id);
      });

      var promise = client.addToMany(activity, feed_ids);

      // Retry 3 times
      for (let i = 0; i < 3; i++) {
         promise = promise.catch(function () {
            return client.addToMany(activity, feed_ids);
         });
      }

      promise.catch(function (err) {
         log.error('Failed to add getstream activity', activity, err);
         throw err;
      });

      return promise;
   }

};

module.exports = getstream;
