var config = require('config'),
   elasticSearch = require('elasticsearch'),
   log = require('../logging.js').logger,
   _ = require('underscore');

var conf = config.get('elastic.connection');
var client = new elasticSearch.Client(conf);
var wrapper = {};
var indexExists = false;

// uncomment below for debugging
// conf.log = 'trace';

function initElasticIndex(idx, cb) {
   // TODOwh: add a check for the index before trying to create
   log.info('Trying to create elastic index...');
   client.indices.create({index: idx}, function() {
      indexExists = true;
      cb();
   });
}

wrapper.putMapping = function putMapping(index, type, mapping) {
   client.indices.putMapping({
      index: index,
      type: type,
      body: mapping
   }, function (err, res) {
      if (err) log.error(err);
   });
};

wrapper.createIndex = function (id, index, type, data, callback) {
   client.create({
      index: index,
      type: type,
      id: id,
      body: data
   }, function (err, response) {
      if (err) log.error('Failed to create ' + type + ' for ' + id, err);
      else log.debug('Created ' + type + ' for ' + id);

      callback(err, response);
   });
};

wrapper.createOrAddToIndex = function (doc, index, type, fields, callback) {
   var elasticData = {};

   // Only send the fields we explicitly define in the model
   _.map(fields, function(val, key) {
      elasticData[key] = doc[key];
   });

   var id = doc.id;

   if ('suggest' in fields) {
      elasticData.suggest = {
         input: doc.name,
         output: doc.name,
         payload: {
            id: id
         }
      };
   }

   elasticData.id = id;

   client.index({
      index: index,
      type: type,
      id: id,
      body: elasticData
   }, function (err, response) {
      if (err) log.error('Failed to create or update ' + type + ' for ' + id, err);
      else log.debug('Created or updated elastic index for ' + type + ' ' + id, elasticData);

      callback(err, response);
   });
};


wrapper.deleteIndex = function (id, index, type, callback) {
   client.delete({
      index: index,
      type: type,
      id: id
   }, function (err, response) {
      if (err) log.error('Failed to delete ' + type + ' ' + id, err);
      else log.debug('Deleted elastic index ' + type + ' ' + id);

      callback(null, response);
   });
};

wrapper.search = function (queryFunction, typeName, query, callback) {
   var esQuery = {
      index: typeName,
      type: typeName,
      body: query
   };

   log.debug('Elasticsearch query', esQuery);

   client[queryFunction](esQuery, callback);
};

module.exports = wrapper;
