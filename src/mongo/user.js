var bcrypt = require('bcrypt'),
   mongoose = require('mongoose'),
   userModel = require('../models/user.js'),
   deleted = require('./plugins/deleted.js'),
   geocode = require('./plugins/geocode.js'),
   constants = require('../constants.js'),
   _ = require('underscore'),
   SALT_WORK_FACTOR = 10;

var type = userModel.name;
var userSchema = new mongoose.Schema(userModel.mongoose);

require('./_base')(userSchema, userModel);

userSchema.plugin(deleted);
userSchema.plugin(geocode);

userSchema.path('email')
   .validate(function validateEmail(email) {
      return constants.EMAIL_REGEX.test(email);
   }, 'Email format is incorrect')
   .validate(function validateEmailUnique(email, cb) {
      var _this = this;
      _this.constructor.findOne({
         email: email
      }, function (err, user) {
         if (err) return cb(false);

         if (user) {
            if (user.id == _this._id) return cb(true);
            return cb(false);
         }
         return cb(true);
      });
   }, 'Email already used');

userSchema.path('zip').validate(function validateZip(zip) {
   return constants.ZIP_REGEX.test(zip);
}, 'Zip is not a 5 digit number');

userSchema.path('password').validate(function validatePassword(password) {
   return password && password.length >= 8;
}, 'Password not long enough');

// Generate hashed password before saving
userSchema.pre('save', function (next) {
   var user = this;

   if (!(user.isNew || user.isModified('password')))
      return next();

   // generate a salt
   bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
      if (err) return next(err);

      // hash the password using our new salt
      bcrypt.hash(user.password, salt, function (err, hash) {
         if (err) return next(err);

         // override the cleartext password with the hashed one
         user.password = hash;
         next();
      });
   });
});

userSchema.methods.comparePassword = function (candidatePassword, callback) {
   bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
      if (err) {
         //TODO handle error
         return callback(err);
      }

      callback(null, isMatch);
   });
};

userSchema.virtual('is_artist').get(function () {
   return _.union(this.admin_artists, this.owned_artists).length > 0;
});

userSchema.virtual('is_venue').get(function () {
   if(this.email === "dork@tunecakes.com") return true;
});

module.exports = mongoose.model(type, userSchema);
