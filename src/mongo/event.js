var mongoose = require('mongoose'),
   elastic = require('./plugins/elastic.js'),
   eventElastic = require('../elastic/event.js'),
   eventModel = require('../models/event.js'),
   deleted = require('./plugins/deleted.js'),
   geocode = require('./plugins/geocode.js'),
   linkedArtistPlugin = require('./plugins/artists.js'),
   constants = require('../constants.js'),
   util = require('../util/general.js');

var type = eventModel.name;
var eventSchema = new mongoose.Schema(eventModel.mongoose);

require('./_base')(eventSchema, eventModel);

eventSchema.plugin(linkedArtistPlugin, {
   field: 'artists'
});

eventSchema.plugin(elastic, {
   index: eventElastic.index,
   type: eventElastic.type,
   fields: eventElastic.properties,
   populate: eventModel.populate
});

eventSchema.plugin(deleted);
eventSchema.plugin(geocode);

eventSchema.path('zip').validate(function validateZip(zip) {
   return constants.ZIP_REGEX.test(zip);
}, 'Zip is not a 5 digit number');


eventSchema.pre('remove', function (next) {
   util.removeReferences('artist', 'events', this._id, next);
   util.removeReferences('user', 'admin_events', this._id, next);
   util.removeReferences('user', 'owned_events', this._id, next);
});

module.exports = mongoose.model(type, eventSchema);
