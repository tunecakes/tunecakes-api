var mongoose = require('mongoose');

// Pending password resets expire after 24 hours
var resets = {
   user_id: { type: String, required: true, unique: true, index: true },
   token: { type: String, required: true, unique: true },
   created: { type: Date, expires: 24 * 3600 }
};

var resetSchema = new mongoose.Schema(resets);

module.exports = mongoose.model('pending_password_reset', resetSchema);
