var uniqueValidator = require('mongoose-unique-validator'),
   conf = require('config'),
   mongoose = require('mongoose'),
   _ = require('underscore'),
   Schema = mongoose.Schema;


mongoose.Collection.prototype.getModelName = function getModelName() {
   var models = this.conn.models;

   for (var i in models) {
      var model = models[i];
      if (model.collection === this) {
         return model.modelName;
      }
   }
};

module.exports = function (schema, model) {
   schema.virtual('_type').get(function () {
      return this.collection.getModelName();
   });

   schema.virtual('href').get(function () {
      return '/api/' + this.collection.getModelName() + '/' + (this.alias || this._id);
   });

   schema.pre('validate', function (next) {
      var model = this;

      // turn populated subdocs into ids
      _.forEach(model, function (value, key, cb) {
         if (value instanceof Array && _.every(value, x => x.id))
            model[key] = _.pluck(value, 'id');
         else if (value instanceof Object && value.id)
            model[key] = value.id;
      });

      next();
   });

   if (model.populate) {
      schema.post('save', function (doc, next) {
         doc.populate(model.populate, function () {
            next();
         });
      });
      // TODO: sadly, the populate here can only take in
      // strings and not the more complex objects. try to
      // fix later so we can remove populate from getBy*
      // methods
      // schema.pre('find', function (next) {
      //    this.populate(model.populate, function () {
      //       next();
      //    });
      // });
   }

   schema.pre('save', function (next) {
      if (this.isNew) this.created = new Date();
      this.updated = new Date();
      next();
   });

   schema.set('toObject', {
      virtuals: true
   });

   schema.set('toJSON', {
      virtuals: true,
      transform: function (doc, ret, options) {
         ret.id = doc.id;
         delete ret._id;
         delete ret.__v;
         delete ret.password;
         return ret;
      }
   });

   schema.plugin(uniqueValidator);

   _.forEach(model.compound_index, function (index) {
      schema.index(index);
   });
};
