var mongoose = require('mongoose');

var deleteSchema = new mongoose.Schema({}, {
   strict: false,
   autoIndex: false
});

module.exports = mongoose.model('delete', deleteSchema);
