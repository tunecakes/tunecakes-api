var mongoose = require('mongoose'),
   artistModel = require('../models/artist.js'),
   deleted = require('./plugins/deleted.js'),
   elastic = require('./plugins/elastic.js'),
   artistElastic = require('../elastic/artist.js'),
   geocode = require('./plugins/geocode.js'),
   constants = require('../constants.js'),
   util = require('../util/general.js');

var type = artistModel.name;
var artistSchema = new mongoose.Schema(artistModel.mongoose);

require('./_base')(artistSchema, artistModel);

artistSchema.plugin(elastic, {
   index: artistElastic.index,
   type: artistElastic.type,
   fields: artistElastic.properties,
   populate: artistModel.populate
});
artistSchema.plugin(deleted);
artistSchema.plugin(geocode);

artistSchema.virtual('is_generated').get(function () {
   var defaultUserId = require('../util/default_user.js').defaultUserId;

   if (!this.owner) return;
   if (this.owner === defaultUserId || this.owner.id === defaultUserId) {
      return true;
   }
});

// This is set dynamically in the artistHandler.injectFollowCount
artistSchema.virtual('followers').get(() => { return this.followers; });
artistSchema.virtual('followers').set((count) => { return this.followers = count; });

artistSchema.path('zip').validate(function validateZip(zip) {
   return constants.ZIP_REGEX.test(zip);
}, 'Zip is not a 5 digit number');

artistSchema.path('tags').validate(function validateTags(tags) {
   return tags.length <= 100;
}, 'Five tags max');

artistSchema.path('tags').validate(function validateTags(tags) {
   for (var i = 0; i < tags.length; i++)
      if (!constants.TAGS_REGEX.test(tags[i])) return false;

   return true;
}, 'Tags can only contain letters and numbers with words separated by a period.');

artistSchema.path('alias').validate(function validateAlias(alias) {
   return constants.ALIAS_REGEX.test(alias);
}, 'Alias can only contain letters and numbers.');

artistSchema.path('contact_email').validate(function validateContactEmail(contact_email) {
   return constants.EMAIL_REGEX.test(contact_email);
}, 'Contact email must be an email');

artistSchema.pre('remove', function removeArtist(next) {
   util.removeReferences('user', 'admin_artists', this._id, next);
   util.removeReferences('user', 'owned_artists', this._id, next);
});

module.exports = mongoose.model(type, artistSchema);
