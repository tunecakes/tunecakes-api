var mongoose = require('mongoose'),
   reminderModel = require('../models/reminder.js'),
   constants = require('../constants');

var type = 'reminder';
var inactiveType = 'inactive-' + type;
var reminderSchema = new mongoose.Schema(reminderModel);

require('./_base')(reminderSchema, reminderModel);

reminderSchema.post('remove', function (doc) {
   var InactiveReminders = mongoose.model(inactiveType);
   new InactiveReminders(this.toObject()).save();
});

module.exports = {
   active: mongoose.model(type, reminderSchema),
   inactive: mongoose.model(inactiveType, reminderSchema)
};
