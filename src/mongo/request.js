var mongoose = require('mongoose'),
   requestModel = require('../models/request.js');

var type = requestModel.name;
var requestSchema = new mongoose.Schema(requestModel.mongoose);

require('./_base')(requestSchema, requestModel);
module.exports = mongoose.model(type, requestSchema);
