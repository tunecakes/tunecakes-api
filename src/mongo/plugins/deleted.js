var Deleted = require('../delete');
module.exports = function deletedPlugin(schema) {
   schema.post('remove', function (doc) {
      var obj = this.toObject({
         depopulate: true,
         virtuals: true
      });
      obj.deleted = new Date;

      new Deleted(obj).save();
   });
};
