var elasticWrapper = require('../../util/elastic_wrapper'),
   log = require('../../logging.js').logger,
   _ = require('underscore');

module.exports = function elasticPlugin(schema, options) {
   schema.post('save', function (doc) {
      function saveIndex(obj) {
         var json_doc = obj.toJSON();

         log.debug('Saving index', json_doc);

         if (options.type === 'event') {
            json_doc.tags = json_doc.tags || [];
            json_doc.linked_artists.forEach(function(a) {
               json_doc.tags = json_doc.tags.concat(a.tags || []);
            });
         }

         elasticWrapper.createOrAddToIndex(
           json_doc, options.index, options.type, options.fields, function (err, data) {
            if (err) log.warn('Elastic error', err);
         });
      }

      doc.populate(options.populate, function(err, pdoc) {
         saveIndex(doc);
      });

      return;
   });

   schema.post('remove', function (doc) {
      elasticWrapper.deleteIndex(doc.id, options.index, options.type, function (err) {
         if (err) log.warn(err);
      });
   });
};
