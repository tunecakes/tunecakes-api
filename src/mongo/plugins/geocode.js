var geocode = require('../../util/geocode'),
   log = require('../../logging.js').logger,
   mongoose = require('mongoose');

// We need this for mongoose.Schema.Types.Point
require('mongoose-geojson-schema');

module.exports = function(schema, options) {
   schema.add({location: {lon: Number, lat: Number}});
   schema.add({mongo_location: { type: mongoose.Schema.Types.Point, coordinates: [Number] }});
   schema.add({address: String});
   schema.add({city: String});
   schema.add({state: String});
   schema.add({country: String});
   schema.add({zip: String});

   schema.post('init', function (doc) {
      if(!doc.location || !doc.location.lat) {
         return;
      }

      if(doc.mongo_location
         && doc.mongo_location.coordinates
         && doc.mongo_location.coordinates.length == 2
         && doc.mongo_location.coordinates[0] === doc.location.lon
         && doc.mongo_location.coordinates[1] === doc.location.lat) {
         return;
      }

      doc.setGeoPoint(doc.location.lat, doc.location.lon);
      doc.save({ validateBeforeSave: false });
   });

   schema.methods.setGeoPoint = function (lat, lon) {
      this.location.lat = lat;
      this.location.lon = lon;
      this.mongo_location = {
         type: "Point",
         coordinates: [lon, lat] 
      };
   };

   function getLocationString(self) {
      var addressString;

      if(self.address) {
         addressString = self.address;
      }

      if (self.city && addressString) {
         addressString += ', ' + self.city;
      } else if (self.city) {
         addressString = self.city;
      }

      if (self.state && addressString) {
         addressString += ', ' + self.state;
      } else if (self.state) {
         addressString = self.state;
      }

      if (self.country && addressString) {
         addressString += ', ' + self.country;
      } else if (self.country) {
         addressString = self.country;
      }

      if (self.zip && addressString) {
         addressString += ', ' + self.zip;
      } else if (self.zip) {
         addressString = self.zip;
      }

      return addressString;
   }

   function populateLatLon(self, cb) {
      var addressString = getLocationString(self);

      self.hasGeoLocationBeenRun = true;

      geocode.googleZipToGeo(addressString)
      .then(location => {
          self.setGeoPoint(location.lat, location.lon);
          cb();
      })
      .catch(err => {
          log.warn('Unable to geocode this model', self, err);
          cb();
      });
   }

   function runGeoLocator(self) {
      if(self.hasGeoLocationBeenRun) return false;
      if(self.isModified('location.lat') && self.isModified('location.lat')) {
         // They gave us a lat/lon, no need to run locator
         return false;
      }

      if((!self.location || !self.location.lat || !self.location.lon) &&
         (self.address || self.city || self.state || self.country || self.zip)) {
         // We haven't geocoded this yet. Let's try
         return true;
      }

      // Has any part of the address changed? If so, run the locator
      return self.isModified('address') ||
         self.isModified('city') ||
         self.isModified('state') ||
         self.isModified('country') ||
         self.isModified('zip');
   }

   schema.pre('save', function(next) {
      var self = this;

      if (runGeoLocator(self)) {
         populateLatLon(self, next);
      } else {
         next();
      }
   });
};
