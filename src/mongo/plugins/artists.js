var _ = require('underscore'),
   util = require('../../util/general.js');

function linkedArtistPlugin(schema, options) {
   var field_name = options.field || 'artists';

   schema.virtual(field_name).get(function () {
      return _.union(this.linked_artists, this.unlinked_artists);
   }).set(function (artists) {
      this.unlinked_artists = _.filter(artists, function (artist) {
         return !util.isObjectId(artist) && !util.isObjectId(artist.id);
      });

      this.linked_artists = _.map(artists, function (artist) {
         if (util.isObjectId(artist)) return artist;
         else if (util.isObjectId(artist.id)) return artist.id;
      }).filter(x => x);
   });
}

module.exports = linkedArtistPlugin;