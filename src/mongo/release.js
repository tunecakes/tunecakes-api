var mongoose = require('mongoose'),
   releaseModel = require('../models/release.js'),
   deleted = require('./plugins/deleted.js'),
   linkedArtistPlugin = require('./plugins/artists.js'),
   util = require('../util/general.js');

var type = releaseModel.name;
var releaseSchema = new mongoose.Schema(releaseModel.mongoose);

require('./_base')(releaseSchema, releaseModel);

releaseSchema.plugin(linkedArtistPlugin, {
   field: 'contributing_artists'
});

releaseSchema.plugin(deleted);

releaseSchema.pre('remove', function (next) {
   util.removeReferences('artist', 'releases', this._id, next);
   util.removeReferences('artist', 'release_contributions', this._id, next);
   util.removeReferences('user', 'admin_releases', this._id, next);
   util.removeReferences('user', 'owned_releases', this._id, next);
});

module.exports = mongoose.model(type, releaseSchema);
