var mongoose = require('mongoose'),
   songModel = require('../models/song.js'),
   deleted = require('./plugins/deleted.js');
//   elastic = require('./plugins/elastic.js');

var type = songModel.name;
var songSchema = new mongoose.Schema(songModel.mongoose);

require('./_base')(songSchema, songModel);

//songSchema.plugin(elastic, {
//   type: type,
//   fields: songModel.elastic,
//   populate: songModel.populate
//});
songSchema.plugin(deleted);

module.exports = mongoose.model(type, songSchema);
