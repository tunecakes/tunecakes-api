var mongoose = require('mongoose'),
   postModel = require('../models/post.js');

var type = postModel.name;
var postSchema = new mongoose.Schema(postModel.mongoose);

require('./_base.js')(postSchema, postModel);

module.exports = mongoose.model(type, postSchema);