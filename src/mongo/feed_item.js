var mongoose = require('mongoose'),
   feedItemModel = require('../models/feed_item.js');

var type = feedItemModel.name;
var feedItemSchema = new mongoose.Schema(feedItemModel.mongoose);

feedItemSchema.index({
   feed: 1,
   time: 1
});

module.exports = mongoose.model(type, feedItemSchema);
