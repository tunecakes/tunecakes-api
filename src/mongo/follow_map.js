var mongoose = require('mongoose'),
   followMapModel = require('../models/follow_map.js');

var type = followMapModel.name;
var followMapSchema = new mongoose.Schema(followMapModel.mongoose);

followMapSchema.index({
   follower: 1,
   publisher: 1
});

module.exports = mongoose.model(type, followMapSchema);
